// プラグインの読み込み
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
//const browsersync = require('browser-sync');

// 実行タスクの登録
gulp.task('default', ['watch']);
gulp.task('final', ['autoprefixer']);


// -------------------------------------watchタスク
//const assetsFiles = ['./assets/*.html','./assets/css/*.css','./assets/js/*.js'];
gulp.task('watch', function(){
  gulp.watch('develop/sass/**/*.scss', ['sass']);
  //gulp.watch(assetsFiles, ['reload']);
});

// -------------------------------------sassタスク
const sassFiles = ['develop/sass/**/*.scss', '!develop/sass/**/_*.scss'];
const cssDir = 'kjournal2/css';

gulp.task('sass', function(){
  gulp.src(sassFiles)
    .pipe(sourcemaps.init())
    .pipe(sass({
      // * expanded:通常、compressed:最小
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(cssDir));
});

// -------------------------------------autoprefixerタスク
const cssFiles = 'kjournal2/css/**/*.css';

gulp.task('autoprefixer', function(){
  gulp.src(cssFiles)
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ie >= 11', 'Android >= 4'],
      grid: true
    }))
    .pipe(gulp.dest('dist'));
});

// -------------------------------------browser-syncタスク
//gulp.task('server',function(){
//  browsersync({
//    server:{
//      baseDir:'./assets/',
//      index: 'index.html'
//    }
//  })
//});
//gulp.task('reload', function () {
//  browsersync.reload();
//});