<?php get_header(); ?>
<?php global $ARTICLE_TOP_URL;?>
<main>
  <div class="main-contents">

    <div class="l-cont">
      <div class="l-cont_text"><p class="no-post"><?php _e('お探しのページは見つかりませんでした。'); ?></p></div>
      <div class="l-cont_back">
        <a href="<?= $ARTICLE_TOP_URL;?>" class="btn -back">高齢者住宅ジャーナルTOPに戻る</a>
      </div>
    </div>
  </div>

    <?php get_sidebar(); ?>
  </main>
<?php get_footer(); ?>