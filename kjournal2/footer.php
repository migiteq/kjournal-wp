<?php
global $ARTICLE_TOP_URL;
$satsuki_url = "https://www.satsuki-jutaku.jp/";
?>
<p id="pageTop"><a href="#">上へ</a></p>

  <footer>
    <div id="footer-wrapper">
      <p class="page-top pc-only">
        <a href="#" style="display: block; position: relative; width: 112px; height: 25px;">
          <img src="<?php bloginfo('template_directory'); ?>/images/page_top_off.gif" width="112" height="25" alt="ページの先頭へ" class="off" style="position: absolute; left: 0px; top: 0px;">
          <img src="<?php bloginfo('template_directory'); ?>/images/page_top_on.gif" width="112" height="25" alt="ページの先頭へ" class="on" style="opacity: 0; position: absolute; left: 0px; top: 0px;">
        </a>
      </p>
      <div id="banner-box" class="pc-only">
        <ul class="mark" style="margin-left:0;">
        <li style="background-image:none; padding-left:0; font-size:11px;">関連リンク</li>
        <li><a href="http://www.mlit.go.jp/" target="_blank">国土交通省</a></li>
        <li><a href="http://www.mhlw.go.jp/" target="_blank">厚生労働省</a></li>
        <li><a href="http://www.shpo.or.jp/" target="_blank">一般社団法人　高齢者住宅協会</a></li>
        </ul>
      </div>
      <div id="footer">
        <p class="pc-only"><a href="<?= $satsuki_url;?>">ホーム </a> ｜ <a href="<?= $satsuki_url;?>system.html">制度について</a> ｜ <a href="<?= $satsuki_url;?>search/index.php">登録住宅をさがす</a> ｜ <a href="<?= $satsuki_url;?>apply.html">事業者の方へ</a> ｜ <a href="<?= $satsuki_url;?>local_government.html">地方公共団体の方へ</a> ｜ <a href="<?= $satsuki_url;?>news/list_all.html">お知らせ</a> ｜ <a href="<?= $satsuki_url;?>faq.php">よくあるご質問</a> </p>
        <p class="pc-only"><a href="<?= $satsuki_url;?>policy.html">プライバシーポリシー</a> ｜ <a href="<?= $satsuki_url;?>inquiry.html">お問い合わせ</a> ｜ <a href="<?= $satsuki_url;?>link.html">リンク集</a> ｜ <a href="<?= $satsuki_url;?>sitemap.html">サイトマップ</a> ｜ <a href="<?= $satsuki_url;?>journal/">お役立ちガイド</a></p>
        <p> Copyright（C） 一般社団法人 高齢者住宅協会. All Rights Reserved </p>
      </div>
    </div>
  </footer>
<?php if ( is_single() ) : ?>
  <!-- /SNSボタン -->
  <script>
    function sns_window( item, height, width ){
    var size = 'menubar=no, toolbar=no, resizable=yes, scrollbars=yes, height='+height+', width='+width;
    window.open( item.href, '_blank', size );
    return false;
  }
  </script>
<?php endif; ?>
  <!-- /object-fit-->
  <script src="<?= get_template_directory_uri(); ?>/js/ofi.min.js"></script>
  <script>
    objectFitImages('img.object-fit_img');
  </script>
  
  <script src="<?= get_template_directory_uri(); ?>/js/jquery-3.3.1.min.js"></script>
  <script src="<?= get_template_directory_uri(); ?>/js/script.js?20201105" type="text/javascript"></script>

<?php wp_footer(); ?>
</body>
</html>