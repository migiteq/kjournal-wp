<?php
clearstatcache ();
//記事トップのURLをグローバル変数化
add_action('after_setup_theme', 'global_variables');
function global_variables(){
	global $ARTICLE_TOP_URL;
	$ARTICLE_TOP_URL = make_root_path(esc_url(home_url('/')));
}

add_filter( 'tiny_mce_before_init', 'override_mce_options' );


//投稿画面のメニュー名を変更
add_filter( 'gettext_with_context', 'custom_gettext_with_context_translation' );
function custom_gettext_with_context_translation( $translation ) {
	$translation = str_ireplace( 'アイキャッチ画像', '任意のサムネイル画像', $translation );
	return $translation;
}


//管理画面のコメントメニューを削除
add_action( 'admin_menu', 'remove_menus' );
function remove_menus(){
	remove_menu_page( 'edit-comments.php' );// コメント
}

//寄稿者権限に画像投稿を許可
if ( current_user_can('contributor') && !current_user_can('upload_files') ){
	add_action('admin_init', 'allow_contributor_uploads');
}
function allow_contributor_uploads() {
	$contributor = get_role('contributor');
	$contributor->add_cap('upload_files');
}

//ルートパスの整形
function make_root_path($url){
	$r = wp_make_link_relative($url);
	if(strpos($url, 'localhost') === false){
		$r = "/journal".$r;
	} else {
		$r = 'http://localhost'.wp_make_link_relative($url);
	}

	return $r;
}

//投稿画面ウィジェット削除
add_action('admin_menu','remove_post_metaboxes');
function remove_post_metaboxes() {
	$post_type = ['post'];
	remove_meta_box( 'postcustom','post','normal' ); // カスタムフィールド
	remove_meta_box( 'postexcerpt','post','normal' ); // 抜粋
	remove_meta_box( 'commentstatusdiv',$post_type,'normal' ); // ディスカッション
	remove_meta_box( 'commentsdiv','post','normal' ); // コメント
	remove_meta_box( 'trackbacksdiv','post','normal' ); // トラックバック
	remove_meta_box( 'authordiv','post','normal' ); // 作成者
	remove_meta_box( 'slugdiv','post','normal' ); // スラッグ
	remove_meta_box( 'cfs_input_685',$post_type,'side' ); // 訪問カウンター
}

//====================================================サイト内検索の範囲に、カテゴリー名、タグ名、を追加
add_filter('posts_search','custom_search', 10, 2);
function custom_search($search, $wp_query) {
	global $wpdb;
//サーチページ以外は終了
	if (!$wp_query->is_search) {return $search;}
	if (!isset($wp_query->query_vars)) {return $search;}

//検索対象追加
	$search_words = explode(' ', isset($wp_query->query_vars['s']) ? $wp_query->query_vars['s'] : '');
	if (count($search_words) > 0) {
		$search = '';
		foreach ($search_words as $word) {
			if (!empty($word)) {
				$search_word = $wpdb->escape("%{$word}%");
				$search .= " AND (
           {$wpdb->posts}.post_title LIKE '{$search_word}'
           OR {$wpdb->posts}.post_content LIKE '{$search_word}'
           OR {$wpdb->posts}.post_author IN (
             SELECT distinct ID
             FROM {$wpdb->users}
             WHERE display_name LIKE '{$search_word}'
             )
           OR {$wpdb->posts}.ID IN (
             SELECT distinct r.object_id
             FROM {$wpdb->term_relationships} AS r
             INNER JOIN {$wpdb->term_taxonomy} AS tt ON r.term_taxonomy_id = tt.term_taxonomy_id
             INNER JOIN {$wpdb->terms} AS t ON tt.term_id = t.term_id
             WHERE t.name LIKE '{$search_word}'
           OR t.slug LIKE '{$search_word}'
           OR tt.description LIKE '{$search_word}'
           )
       ) ";
			}
		}
	}
	return $search;
}

//====================================================投稿画面カテゴリラジオボタン化
function change_category_to_radio() {
	?>
	<script>
		jQuery(function($) {
			// カテゴリーをラジオボタンに変更
			$('#categorychecklist input[type=checkbox]').each(function(i) {
				$this = $(this);
				$clone = $this.clone();
				$this.replaceWith($clone.attr('type', 'radio'));
				if ($this.prop('checked')) $clone.prop('checked', true);
			});
			// 「新規カテゴリーを追加」を非表示
			$('#category-adder').hide();
		});
	</script>
	<?php
}
add_action( 'admin_head-post-new.php', 'change_category_to_radio' );
add_action( 'admin_head-post.php', 'change_category_to_radio' );
//====================================================webAPIカスタム


//エンドポイントの追加
add_action('rest_api_init','add_custom_endpoint');
function add_custom_endpoint() {
	register_rest_route('articles','/posts', array(
		'methods' => 'GET',
		'permission_callback' => 'permissions_check',
		'callback' => 'show_item'
	));
}
//権限チェック
function permissions_check(){
	return ($_GET['key'] === 'teikyosystem');
}
//戻り値整形
function show_item(){
	$param = $_GET;
	$data = array();
	$args = array(
			'orderby'=>$param['sort']?'modified date':'rand',
			'order'=>$param['sort']?:'DESC',
			'posts_per_page'=>$param['count']?:15,
	);
	if(isset($param['pr'])){
		$pr =$param['pr'];
		$args['meta_query'] = array(array(
			'key'=>'pr',
			'value'=>$pr,
			'type'=>'numeric',
			'compare'=>'='
		));
	}

	foreach(get_posts($args) as $post) {
		setup_postdata($post);
		//INIT::記事情報_画像URL（任意でサムネイル画像を表示）
		if (has_post_thumbnail($post->ID)){
			$thumbnail_id = get_post_thumbnail_id($post->ID);
			$img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
		} else {
			$img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
		}
		$this_data = [
			'id'=>$post->ID,
			'date'=>$post->post_date,
			'modified'=>$post->post_modified,
			'pr'=> intval(get_post_meta($post->ID,'pr',true)),
			'link'=>wp_make_link_relative($post->guid),
			'title'=>wp_strip_all_tags($post->post_title),
			'cat_id'=> get_the_category($post->ID)[0]->cat_ID,
			'cat_name'=> get_the_category($post->ID)[0]->cat_name,
			'content'=>array(
				'img'=>wp_make_link_relative($img_url),
				'text'=>get_post_meta($post->ID,'text_mv',true)
			)
		];
		$data[] = $this_data;
	}
	return $data;
}


//====================================================$args生成
function build_args($v){
	$args= array(
			'orderby'=>$v['sort']?'modified date':'rand',
			'order'=>$v['sort']?:'DESC',
			'posts_per_page'=>$v['count']?:4,
	);
	if(isset($v['pr'])){
		$args['meta_query'] = array(array(
				'key'=>'pr',
				'value'=>$v['pr'],
				'type'=>'numeric',
				'compare'=>'='
		));
	}
	if(isset($v['cat'])){
		$args['category'] = $v['cat'];
	}
	if(isset($v['tag'])){
		$args['tax_query'] = array(array(
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => $v['tag']));
	}
	if(isset($v['paged'])){
		$args['paged'] = $v['paged'];
	}
	if(isset($v['exclude'])){
		$args['exclude'] = array($v['exclude']);
	}
	if(isset($v['ranking'])){
		$args['meta_query'] = array(array(
			'key'=>'post_view_count',
			'type'=>'numeric'
		));
		$args['orderby'] ='meta_value';
	}
	return $args;
}

//==================================================== 三点リーダ

function text_ellipsis($str, $start, $length, $trimmarker = '', $encoding = false) {
	$str = wp_strip_all_tags($str);
	$encoding = $encoding ? $encoding : mb_internal_encoding();
	$str = mb_substr($str, $start, mb_strlen($str), $encoding);
	if (mb_strlen($str, $encoding) > $length) {
		$markerlen = mb_strlen($trimmarker, $encoding);
		$str = mb_substr($str, 0, $length - $markerlen, $encoding) . $trimmarker;
	}
	return $str;
}


//==================================================== アイキャッチ設定
add_theme_support('post-thumbnails');

//==================================================== 画像サイズ設定
add_image_size( 'img_mv', 500, 260, true );

//==================================================== SNSボタン
function pagination($max_num_pages=0) {
	// Don't print empty markup if there's only one page.
	$max_num_pages = $max_num_pages ? (int)$max_num_pages : $GLOBALS['wp_query']->max_num_pages;
	if ( $max_num_pages < 2 ) {
		return;
	}

	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );

	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}

	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

	// Set up paginated links.
	$links = paginate_links( array(
			'base'     => $pagenum_link,
			'format'   => $format,
			'total'    => $max_num_pages,
			'current'  => $paged,
			'mid_size' => 2,
			'add_args' => array_map( 'urlencode', $query_args ),
			'prev_text' => __( '&lt;' ),
			'next_text' => __( '&gt;' ),
	) );

	if ( $links ) :?>
		<nav class="navigation paging-navigation" role="navigation">
			<div class="pagination loop-pagination">
				<?php echo $links; ?>
			</div>
		</nav>
		<?php endif;
}
//==================================================== 更新/公開日
function get_mtime($format) {
	$mtime = get_the_modified_time('Ymd');
	$ptime = get_the_time('Ymd');
	if ($ptime > $mtime) {
		return '公開日：'.get_the_time($format);
	} elseif ($ptime === $mtime) {
		return '公開日：'.get_the_time($format);
	} else {
		return '公開日：'.get_the_time($format).'｜更新日：'.get_the_modified_time($format);
	}
}

//==================================================== 更新日を任意にコントロール
//管理画面が開いたときに実行
add_action( 'admin_menu', 'add_update_level_custom_box' );
//更新ボタンが押されたときに実行
add_action( 'save_post', 'save_custom_field_postdata' );
//「更新」以外は更新日時を変更しない
add_filter( 'wp_insert_post_data', 'my_insert_post_data', 10, 2 );

//------------ カスタムフィールドを投稿画面に追加
function add_update_level_custom_box() {
	//ページ編集画面にカスタムメタボックスを追加
	add_meta_box('update_level','更新日の変更','html_update_level_custom_box','post', 'side','high');
}

//------------ 投稿画面に表示するHTML
function html_update_level_custom_box() {
	//↓ディフォルトで更新するにチェックしたいので、コメントアウト
//	$post = isset($_GET['post']) ? $_GET['post'] :null;
//	$update_level = get_post_meta( $post, 'update_level' );
//	$level = $update_level ? $update_level[0] : null;
	ob_start();
	?>
	<div style="padding-top: 3px; overflow: hidden;">
		<div style="width: 100px; float: left;"><input name="update_level" type="radio" value="high"/>更新する</div>
		<div><input name="update_level" type="radio" value="low" checked="checked"/>更新しない</div>
		<p class="howto" style="margin-top:1em;">更新日時を変更するかどうかを設定します。誤字修正などで更新日を変更したくない場合は「変更しない」にチェックを入れてください。</p>
	</div>
	<?php
	ob_end_flush();
}
//------------ メタボックスの位置を公開の次に表示
add_action('admin_footer', 'my_footer');
function my_footer() {
	ob_start();
?>
	<script type="text/javascript">
		jQuery(function(){
			jQuery("#submitdiv").prependTo(jQuery("#side-sortables"));
		});
	</script>
<?php
	ob_end_flush();
}
//------------ カスタムフィールドの値をDBに書き込む
function save_custom_field_postdata( $post_id ) {
	$mydata = isset($_POST['update_level']) ? $_POST['update_level'] : null;
	if( "" == get_post_meta( $post_id, 'update_level' )) {
		/* update_levelというキーでデータが保存されていなかった場合、新しく保存 */
		add_post_meta( $post_id, 'update_level', $mydata, true ) ;
	} elseif( $mydata != get_post_meta( $post_id, 'update_level' )) {
		/* update_levelというキーのデータと、現在のデータが不一致の場合、更新 */
		update_post_meta( $post_id, 'update_level', $mydata ) ;
	} elseif( "" == $mydata ) {
		/* 現在のデータが無い場合、update_levelというキーの値を削除 */
		delete_post_meta( $post_id, 'update_level' ) ;
	}
}

//------------ 「更新」以外は更新日時を変更しない
function my_insert_post_data( $data, $postarr ){
	$mydata = isset($_POST['update_level']) ? $_POST['update_level'] : null;
	if( $mydata == "low" ){
		unset( $data["post_modified"] );
		unset( $data["post_modified_gmt"] );
	}
	return $data;
}
//==================================================== TDK構築
function init_tdk() {
	$title_base = ' | 高齢者住宅協会';
	$title_journal = ' | 高齢者住宅ジャーナル';
	$seo = array();
if(is_home()){//トップ
	$seo["title"] = '高齢者住宅ジャーナル'.$title_base;
	$seo["desc"] = 'サービス付き高齢者向け住宅（サ高住）・高齢者向け住宅のお役立ち情報 | 高齢者向け住宅への住み替えを検討の方へ、高齢者住宅協会が制度・手続きなど基礎知識や業界情報をご紹介します。';
	$seo["keyword"] = 'サービス付き高齢者向け住宅,高齢者住宅ジャーナル,サ高住,高齢者住宅協会';
} elseif(is_page('allposts')){//最新記事
	$seo["title"] = '新着記事一覧'.$title_journal.$title_base;
	$seo["desc"] = '新着記事を一覧でご紹介。高齢者住宅ジャーナル'.$title_base;
	$seo["keyword"] = 'サービス付き高齢者向け住宅,高齢者住宅ジャーナル,新着,一覧,サ高住,高齢者住宅協会';
} elseif(is_page('summary')){//4つの安心
  $seo["title"] = 'リフォームで目指す４つの安心'.$title_journal.$title_base;
  $seo["desc"] = 'リフォームの大切さがわかったら早めの備えが必要です。健康で元気な今のうちに決断を！｜高齢期の健康で快適な暮らしのための住まいの改修ガイドライン'.$title_base;
  $seo["keyword"] = '高齢期,リフォーム,健康で快適な暮らし,高齢者住宅ジャーナル,新着,一覧,サ高住,高齢者住宅協会';
} elseif(is_page('considerations')){//８つの配慮事項
  $seo["title"] = '８つの配慮事項'.$title_journal.$title_base;
  $seo["desc"] = 'ガイドラインで提唱する「８つの配慮事項」をおさえて今の健康を維持し健康寿命を伸ばしましょう。｜高齢期の健康で快適な暮らしのための住まいの改修ガイドライン'.$title_base;
  $seo["keyword"] = '高齢期,リフォーム,健康で快適な暮らし,配慮事項,高齢者住宅ジャーナル,新着,一覧,サ高住,高齢者住宅協会';
} elseif(is_page('future-map')){//未来図の描き方
  $seo["title"] = '未来図の描き方'.$title_journal.$title_base;
  $seo["desc"] = 'これからの時間は自分たちの人生のために。暮らしの未来図を描くことから始めましょう。｜高齢期の健康で快適な暮らしのための住まいの改修ガイドライン'.$title_base;
  $seo["keyword"] = '高齢期,リフォーム,健康で快適な暮らし,未来図,ビジョンシート,高齢者住宅ジャーナル,新着,一覧,サ高住,高齢者住宅協会';
} elseif(is_page('consultation')){//未来図の描き方
  $seo["title"] = '将来に備えたリフォーム・住み替えはプロに相談しましょう！'.$title_journal.$title_base;
  $seo["desc"] = '将来の暮らし方のこと、そろそろ専門家に相談してみませんか？将来に備えたリフォーム・住み替えはプロに相談しましょう。高齢期に起こるあなたと住まいの変化について、エキスパートの相談員があなたの想いを受け止めて、早めの備えのことを一緒に考えます。どんなお悩みでもお気軽にご相談ください。'.$title_base;
  $seo["keyword"] = '高齢期,リフォーム,健康で快適な暮らし,相談,相談予約,高齢者住宅ジャーナル,新着,一覧,サ高住,高齢者住宅協会';
} elseif(is_single()){//記事詳細
	global $post;
	$readtext = text_ellipsis(get_post_meta($post->ID,'text_mv',true), 0, 150, "…", "UTF-8");
	$text = str_replace(array("\r", "\n"), '', $readtext);
	$catname = get_the_category($post->ID)[0]->cat_name;
	$seo["title"] = $post->post_title.$title_journal.$title_base;
	$seo["desc"] = $text;
	$seo["keyword"] = 'サービス付き高齢者向け住宅,高齢者住宅ジャーナル,'.$catname.',タグ名,サ高住,高齢者住宅協会';

} elseif(is_category()){//カテゴリ
	global $cat;
	$catInfo = get_category($cat);
	$seo["title"] = '「'.$catInfo->cat_name.'」記事一覧'.$title_journal.$title_base;
	$seo["desc"] = $catInfo->description.' 高齢者住宅ジャーナル'.$title_base;
	$seo["keyword"] = 'サービス付き高齢者向け住宅,高齢者住宅ジャーナル,'.$catInfo->cat_name.',一覧,サ高住,高齢者住宅協会';

} elseif(is_tag()){//タグ
	global $tag;
	$args = array('slug'=>$tag);
	$tagInfo = get_tags($args)[0];
	$seo["title"] = '「'.$tagInfo->name.'」記事一覧'.$title_journal.$title_base;
	$seo["desc"] = $tagInfo->name.'に関する記事を一覧でご紹介。高齢者住宅ジャーナル'.$title_base;
	$seo["keyword"] = 'サービス付き高齢者向け住宅,高齢者住宅ジャーナル,'.$tagInfo->name.',一覧,サ高住,高齢者住宅協会';

} elseif(is_search()){//検索
	global $s;
	$seo["title"] = '「'.$s.'」記事一覧'.$title_journal.$title_base;
	$seo["desc"] = $s.'に関する記事を一覧でご紹介。高齢者住宅ジャーナル'.$title_base;
	$seo["keyword"] = 'サービス付き高齢者向け住宅,高齢者住宅ジャーナル,'.$s.',一覧,サ高住,高齢者住宅協会';

} else{
	$seo["title"] = '高齢者住宅ジャーナル'.$title_base;
	$seo["desc"] = 'サービス付き高齢者向け住宅（サ高住）・高齢者向け住宅のお役立ち情報 | 高齢者向け住宅への住み替えを検討の方へ、高齢者住宅協会が制度・手続きなど基礎知識や業界情報をご紹介します。';
	$seo["keyword"] = 'サービス付き高齢者向け住宅,高齢者住宅ジャーナル,サ高住,高齢者住宅協会';
}

	return $seo;
}

//==================================================== ランキング表示::訪問数カウントアップ
function setPostViews($postID) {
	$count_key = 'post_view_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		$count = 1;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, $count);
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}
