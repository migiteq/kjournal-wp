<!--?php /** * Template Name: ８つの配慮事項 */ ?-->
<?php get_header(); ?>
<?php
global $ARTICLE_TOP_URL;
?>
<main>
  <!--============================================パンくずリスト-->
  <ul class="breadcrumb">
    <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
    <li><a href="<?= make_root_path(esc_url(home_url('/guideline/future-map')));?>">未来図の描き方</a></li>
    <li>８つの配慮事項</li>
  </ul>

  <div class="main-contents">
    <div class="guide-cont -arrow">
      <!--============================================ページタイトル-->
      <div class="guide-cont_head">
        <h1 class="guide-cont_title"><span class="pc-only">ガイドラインで提唱する</span><span class="text -strong -line">「８つの配慮事項」</span>をおさえて<br>今の<span class="text -strong">健康を維持</span>し健康寿命を伸ばしましょう。</h1>
        <!--============================================目次 -->
        <div class="hairyo-toc">
<!--          <h3 class="hairyo-toc_title">８つの配慮事項</h3>-->
          <ul class="hairyo-toc_list">
            <li class="hairyo-toc_item"><a href="#hairyo01">1.温熱環境への配慮</a></li>
            <li class="hairyo-toc_item"><a href="#hairyo02">2.外出のしやすさへの配慮</a></li>
            <li class="hairyo-toc_item"><a href="#hairyo03">3.トイレ・浴室の利用しやすさへの配慮</a></li>
            <li class="hairyo-toc_item"><a href="#hairyo04">4.日常生活空間の合理化への配慮</a></li>
            <li class="hairyo-toc_item"><a href="#hairyo05">5.主要動線上のバリアフリーへの配慮</a></li>
            <li class="hairyo-toc_item"><a href="#hairyo06">6.設備の導入・更新への配慮</a></li>
            <li class="hairyo-toc_item"><a href="#hairyo07">7.光・音・匂い・湿度などへの配慮</a></li>
            <li class="hairyo-toc_item"><a href="#hairyo08">8.余剰空間の活用への配慮</a></li>
          </ul>
        </div>
      </div>
      <!--============================================本文エリア-->
      <div class="guide-cont_body">
        <!--============================================ 配慮1 -->
        <section class="hairyo-cont" id="hairyo01">
          <h2 class="hairyo-cont_title">温熱環境への配慮</h2>
          <div class="hairyo-cont_body">
            <p class="hairyo-cont_text">温熱環境が整っていないと、ヒートショックの危険があるということはよく言われていますが、トイレがおっくうになったり、生活全般の活動量が低下して、廃用症候群のリスクが高まります。</p>
            <div class="hairyo-point">
              <p class="hairyo-point_title"><span>配慮のポイント</span></p>
              <ul class="hairyo-point_list">
                <li class="hairyo-point_item"><span>①</span>開口部など住宅の断熱性を高め、暖冷房設備を適切に設置</li>
                <li class="hairyo-point_item"><span>②</span>部屋と廊下、トイレ、浴室などで温度差を減らす</li>
                <li class="hairyo-point_item"><span>③</span>住まい全体の暖冷房ができるように間取りなどを工夫</li>
              </ul>
<!--              <div class="hairyo-point_btn"><a class="btn -subLink" href="#">具体的な改修例を見る</a></div>-->
            </div>
            <div class="hairyo-effect">
              <h3 class="hairyo-effect_title">改修後<br class="pc-only">の効果</h3>
              <ul class="hairyo-effect_list">
                <li class="hairyo-effect_item"><span>◎</span>運動機能の維持と健康で自立した期間の延伸</li>
                <li class="hairyo-effect_item"><span>◎</span>血圧低減効果やヒートショック・熱中症の防止</li>
                <li class="hairyo-effect_item"><span>◎</span>室温調節が難しくなった場合の適切な環境確保</li>
                <li class="hairyo-effect_item"><span>◎</span>断熱性や設備効率の向上による光熱費の抑制</li>
              </ul>
            </div>
          </div>
        </section>

        <!--============================================ 配慮2 -->
        <section class="hairyo-cont" id="hairyo02">
          <h2 class="hairyo-cont_title">外出のしやすさへの配慮</h2>
          <div class="hairyo-cont_body">
            <p class="hairyo-cont_text">外に出ることがおっくうになると、買い物に行かなくなったり、<br>人と交わる機会が少なくなって脳が活性化しなくなるなどのリスクが高まります。</p>
            <div class="hairyo-point">
              <p class="hairyo-point_title"><span>配慮のポイント</span></p>
              <ul class="hairyo-point_list">
                <li class="hairyo-point_item"><span>①</span>玄関や勝手口から道路まで安心して移動できるようにする</li>
                <li class="hairyo-point_item"><span>②</span>外出や来訪しやすい玄関とする</li>
                <li class="hairyo-point_item"><span>③</span>玄関の改修が難しい場合、縁側や掃出し窓からバリアフリーの経路を確保する</li>
              </ul>
<!--              <div class="hairyo-point_btn"><a class="btn -subLink" href="#">具体的な改修例を見る</a></div>-->
            </div>
            <div class="hairyo-effect">
              <h3 class="hairyo-effect_title">改修後<br class="pc-only">の効果</h3>
              <ul class="hairyo-effect_list">
                <li class="hairyo-effect_item"><span>◎</span>家族や地域との交流の促進による生活の充実</li>
                <li class="hairyo-effect_item"><span>◎</span>運動機能や意欲の低下を予防し健康で自立した期間の延伸</li>
                <li class="hairyo-effect_item"><span>◎</span>心身機能が衰えた場合にも、外出の容易化</li>
              </ul>
            </div>
          </div>
        </section>

        <!--============================================ 配慮3 -->
        <section class="hairyo-cont" id="hairyo03">
          <h2 class="hairyo-cont_title">トイレ・浴室の利用しやすさへの配慮</h2>
          <div class="hairyo-cont_body">
            <p class="hairyo-cont_text">排泄の自立は、人間の尊厳に関わります。家族が施設入居を考え始めるタイミングは、排泄介助が必要になった時が多いといわれています。</p>
            <div class="hairyo-point">
              <p class="hairyo-point_title"><span>配慮のポイント</span></p>
              <ul class="hairyo-point_list">
                <li class="hairyo-point_item"><span>①</span>夜間にも寝室からトイレまで行きやすい環境を整える</li>
                <li class="hairyo-point_item"><span>②</span>トイレや浴室を安全・安心に利用できるようバリアフリー環境を整える</li>
                <li class="hairyo-point_item"><span>③</span>ヒートショック防止のため、適切な温熱環境を確保する</li>
              </ul>
<!--              <div class="hairyo-point_btn"><a class="btn -subLink" href="#">具体的な改修例を見る</a></div>-->
            </div>
            <div class="hairyo-effect">
              <h3 class="hairyo-effect_title">改修後<br class="pc-only">の効果</h3>
              <ul class="hairyo-effect_list">
                <li class="hairyo-effect_item"><span>◎</span>トイレや浴室の安心・快適性の向上と日常生活のストレスや負担の軽減</li>
                <li class="hairyo-effect_item"><span>◎</span>心身機能が衰えた場合にも自宅で生活できる期間の延伸</li>
              </ul>
            </div>
          </div>
        </section>
        <!--============================================ 配慮4 -->
        <section class="hairyo-cont" id="hairyo04">
          <h2 class="hairyo-cont_title">日常生活空間の合理化への配慮</h2>
          <div class="hairyo-cont_body">
            <p class="hairyo-cont_text">お子様が巣立ってゆくと、２階部屋の使用頻度も少なくなってきます。階段の昇降は日々の負担も大きく、１階だけで生活できるコンパクト空間化がおすすめです。</p>
            <div class="hairyo-point">
              <p class="hairyo-point_title"><span>配慮のポイント</span></p>
              <ul class="hairyo-point_list">
                <li class="hairyo-point_item"><span>①</span>日常的な生活空間を同じ階にする</li>
                <li class="hairyo-point_item"><span>②</span>日常的によく利用するスペースの間仕切り等を少なくし、広々と一定的に利用する</li>
              </ul>
<!--              <div class="hairyo-point_btn"><a class="btn -subLink" href="#">具体的な改修例を見る</a></div>-->
            </div>
            <div class="hairyo-effect">
              <h3 class="hairyo-effect_title">改修後<br class="pc-only">の効果</h3>
              <ul class="hairyo-effect_list">
                <li class="hairyo-effect_item"><span>◎</span>開放的で快適な生活空間の実現</li>
                <li class="hairyo-effect_item"><span>◎</span>運動機能低下の予防と健康で自立した期間の延伸</li>
                <li class="hairyo-effect_item"><span>◎</span>心身機能が衰えた場合にも、自宅での生活継続の容易化</li>
              </ul>
            </div>
          </div>
        </section>

        <!--============================================ 配慮5 -->
        <section class="hairyo-cont" id="hairyo05">
          <h2 class="hairyo-cont_title -small">主要動線上のバリアフリーへの配慮</h2>
          <div class="hairyo-cont_body">
            <p class="hairyo-cont_text">日常生活でよく使う空間上の段差をなくして転倒を防止し、いつまでも安全に</p>
          </div>
        </section>
        <!--============================================ 配慮6 -->
        <section class="hairyo-cont" id="hairyo06">
          <h2 class="hairyo-cont_title -small">設備の導入・更新への配慮</h2>
          <div class="hairyo-cont_body">
            <p class="hairyo-cont_text">最新の設備を導入することで、便利で安全・清潔、しかもランニングコストも低減</p>
          </div>
        </section>
        <!--============================================ 配慮7 -->
        <section class="hairyo-cont" id="hairyo07">
          <h2 class="hairyo-cont_title -small">光・音・匂い・湿度などへの配慮</h2>
          <div class="hairyo-cont_body">
            <p class="hairyo-cont_text">五感が衰えても、長時間にわたって快適に過ごせる室内に</p>
          </div>
        </section>
        <!--============================================ 配慮8 -->
        <section class="hairyo-cont" id="hairyo08">
          <h2 class="hairyo-cont_title -small">余った部屋の活用への配慮</h2>
          <div class="hairyo-cont_body">
            <p class="hairyo-cont_text">子ども部屋や和室を趣味室などにすることで、楽しく充実した日常生活を</p>
          </div>
        </section>

      </div>
    </div>

    <!--============================================CVポイント-->
    <div class="guide-summary">
      <section class="guide-summary_mainBox -summary">
        <h2 class="guide-summary_title">住まいの改修や住み替えの<br class="sp-only">大切さはわかった...<br>でも、いつ決めたらいいの...？</h2>
        <div class="guide-summary_btn">
          <a class="btn -mainLink" href="<?= make_root_path(esc_url(home_url('/guideline/summary')));?>">将来の暮らしに備えるポイントを<br class="sp-only">確認しましょう！</a>
        </div>
<!--        <p class="guide-summary_text">-->
<!--          <span class="pc-only">将来に備えた</span>住まいついての相談はこちらの窓口へ<br>-->
<!--          <a href="#">総合相談の窓口一覧はこちら</a>-->
<!--        </p>-->
      </section>
    </div>

  </div><!-- /.main-contents-->

  <?php get_sidebar(); ?>
  <!--============================================パンくずリスト（PCのみ）-->
  <ul class="breadcrumb -foot sp-only">
    <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
    <li><a href="<?= make_root_path(esc_url(home_url('/guideline/future-map')));?>">未来図の描き方</a></li>
    <li>８つの配慮事項</li>
  </ul>
</main>
<?php get_footer(); ?>
