<!--?php /** * Template Name: 相談相談窓口 */ ?-->
<?php get_header(); ?>
<?php
global $ARTICLE_TOP_URL;
?>
<main>
  <!--============================================パンくずリスト-->
  <ul class="breadcrumb">
    <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
    <li><a href="<?= make_root_path(esc_url(home_url('/guideline/future-map')));?>">未来図の描き方</a></li>
    <li>総合相談窓口</li>
  </ul>

  <div class="main-contents">
    <div class="guide-cont -arrow">
      <!--============================================ページタイトル-->
      <div class="guide-cont_head -consul">
        <p class="consul-subTitle"><span>安心・安全な住まいを実現</span></p>
        <h1 class="guide-cont_title">将来に備えたリフォーム・住み替えは<br><span class="text -strong -line">プロに相談</span>しましょう！</h1>
        <p class="consul-offer">提供 一般社団法人・高齢者住宅協会</p>
      </div>

      <!--============================================本文エリア-->
      <div class="guide-cont_body">
        <section class="consul-intro">
          <h2 class="consul-intro_title">可能な限り、<br class="sp-only">今の住まいで暮らし続けたいですか？</h2>
          <div class="consul-intro_body">
            <picture>
              <source media="(min-width: 481px)" srcset="<?= get_template_directory_uri(); ?>/images/guideline/illust_worries-pc.svg">
              <source media="(max-width: 480px)" srcset="<?= get_template_directory_uri(); ?>/images/guideline/illust_worries-sp.svg">
              <img src="<?= get_template_directory_uri(); ?>/images/guideline/illust_worries-pc.svg" alt="可能な限り、今の住まいで暮らし続けたいですか？">
            </picture>
            <div class="consul-advice">
              <h3 class="consul-advice_title">将来の暮らし方のこと、そろそろ専門家に相談してみませんか？</h3>
              <p class="consul-advice_text">高齢期に起こるあなたと住まいの変化について、エキスパートの相談員が、あなたの思いを受け止めて、早めの備えのことを一緒に考えます。どんなお悩みでもお気軽にご相談ください。</p>
              <div class="consul-advice_links">
                <a class="btn -subLink" href="#consultants">どんな相談員さんがいるの？</a><br>
                <a class="consul-advice_textlink" href="<?= make_root_path(esc_url(home_url('/guideline/future-map')));?>">早めの備えが<span class="pc-only">将来の暮らしに</span>オススメな理由とは？</a>
              </div>
            </div>
          </div>
        </section>
        <section class="summary-cont">
          <h2 class="summary-cont_title">相談予約ってなに？</h2>
          <div class="summary-cont_body">
            <p class="summary-cont_text">一般社団法人高齢者住宅協会は、2019年3月に国から発表された「高齢期の健康で快適な暮らしのための住まいの改修ガイドラインの普及事業を実施しております。この度、同ガイドラインに沿って、プレシニア・アクティブシニアを対象とした居住の場の選択の支援として、「住替えるか」「住み続けるか」総合相談窓口を設置しました。<br>相談員は、高齢者住宅協会が実施する「介護と住まいの相談員」研修受講者を基本とします。</p>
            <ul class="consul-specials">
              <li class="consul-specials_item">
                <div class="consul-specials_text">住み続けか住み替えかの<br class="pc-only">相談ができる<br><span class="text -strong">オンラインの相談窓口</span></div>
              </li>
              <li class="consul-specials_item">
                <div class="consul-specials_text">相談員は<br class="pc-only"><span class="text -strong">各分野のエキスパート</span></div>
              </li>
              <li class="consul-specials_item">
                <div class="consul-specials_text"><span class="text -strong">もちろん無料</span><br class="pc-only">（2021年3月まで）</div>
              </li>
            </ul>
          </div>
        </section>
      </div>

    </div>

    <!--============================================相談員選択-->
    <section class="consul-selConsul">
      <div class="consul-selConsul_head">
        <h2 class="consul-selConsul_title">みなさまのご不安には<br><span class="text -strong">経験豊富なプロの相談員</span><br class="sp-only">がお答えします。</h2>
        <ul class="consul-flow">
          <li class="consul-flow_item">お悩みの内容から<br>相談したい相談員を選択</li>
          <li class="consul-flow_item">相談予約フォームから<br>お悩みの内容などを入力</li>
          <li class="consul-flow_item">オンラインにて相談</li>
        </ul>
      </div>
      <div class="consul-selConsul_body" id="consultants">
        <article class="consul-person">
          <div class="consul-person_img">
            <img src="<?= get_template_directory_uri(); ?>/images/guideline/consultant/imai.jpg" alt="株式会社ニューライフフロンティア・今井紀子">
          </div>
          <div class="consul-person_detail">
            <div class="consul-person_profile">
              <h3 class="consul-person_name"><ruby class="consul-person_name_ruby" data-ruby="いまい">今井</ruby> <ruby class="consul-person_name_ruby" data-ruby="のりこ">紀子</ruby></h3>
              <p class="consul-person_com">【介護情報館】<br class="sp-only">株式会社ニューライフフロンティア</p>
              <p class="consul-person_part">病院副事務長 / 老人ホーム施設長 / 老健施設事務長 / 療養型病院相談員 / 高齢者住宅協会『介護と住まいの相談員』 / 訪問介護員2級養成研修課程修了 / 終活カウンセラー / 産業カウンセラー</p>
            </div>
            <dl class="consul-person_textBox">
              <dt class="consul-person_textBox_title">対応できる相談内容</dt>
              <dd class="consul-person_textBox_data">
                <ul class="consul-person_possible">
                  <li>高齢者住宅への住み替えのご相談</li>
                  <li>在宅介護に必要な資源やお悩み、お困りごとのご相談</li>
                  <li>後見人、生前整理事業者をお探しの方の相談</li>
                </ul>
              </dd>
            </dl>
            <dl class="consul-person_textBox js-consultantMessage">
              <dt class="consul-person_textBox_title is-spLink">相談員からのメッセージ</dt>
              <dd class="consul-person_textBox_data">
                <h4 class="consul-person_message_title">消費者には医療や介護の現場の情報は伝わりにくく、“住み替える”か“住み続けるか”の判断に悩む高齢者は多くいます。正確な情報と多くの相談事例をもとにご助言致します。</h4>
                <p class="consul-person_message_text">年間４百件〜１千件以上の“終の棲家”を探される方や、「何としても自宅で看取られたい」とお考えのご相談者には、数時間、中には数年かけてお話をお聞きし、慣れ親しんだ自宅に住み続けるか、住み替えるかのご不安を軽減した相談対応を行っております。又、医療介護の現場経験をもとに、行政セミナーや大手新聞社主催の講演会等で、解説本やランキング本には載らない「高齢者住宅はこう選ぶ」、「住み続ける為に知っておくこと」等を年間数十回開催致しております。</p>
              </dd>
            </dl>
            <div class="consul-person_btn">
              <a href="https://demo-shpo.resv.jp/direct_calendar.php?direct_id=1" class="btn -selConsul">この相談員さんに相談する</a>
            </div>
          </div>
        </article>
        <article class="consul-person">
          <div class="consul-person_img">
            <img src="<?= get_template_directory_uri(); ?>/images/guideline/consultant/koizumi.jpg" alt="株式会社マザアス・小泉真由美">
          </div>
          <div class="consul-person_detail">
            <div class="consul-person_profile">
              <h3 class="consul-person_name"><ruby class="consul-person_name_ruby" data-ruby="こいずみ">小泉</ruby> <ruby class="consul-person_name_ruby" data-ruby="まゆみ">真由美</ruby></h3>
              <p class="consul-person_com">株式会社マザアス</p>
              <p class="consul-person_part">社会福祉士 / 介護福祉士 / 介護支援専門員 / 終活カウンセラー</p>
            </div>
            <dl class="consul-person_textBox">
              <dt class="consul-person_textBox_title">対応できる相談内容</dt>
              <dd class="consul-person_textBox_data">
                <ul class="consul-person_possible">
                  <li>在宅介護等に関わる相談全般</li>
                </ul>
              </dd>
            </dl>
            <dl class="consul-person_textBox js-consultantMessage">
              <dt class="consul-person_textBox_title is-spLink">相談員からのメッセージ</dt>
              <dd class="consul-person_textBox_data">
                <h4 class="consul-person_message_title">『住み慣れたご自宅で最期まで暮らし続けたい〜』とお考えの方へ、ハード面、ソフト面からのアドバイスさせていただきます！</h4>
                <p class="consul-person_message_text">在宅介護や施設介護での20年の幅広い現場経験を経て、特に認知症ケアはじめ、介護が必要な80.90.100歳になられたご本人ご家族への数多くのアプローチ経験をもとに、これからの高齢期の暮らし方選択を悩まれている方々へ、事前に準備しておくことの大切さについて、お役に立つかはありますが、相談に頂ければ幸いです！</p>
              </dd>
            </dl>
            <div class="consul-person_btn">
              <a href="https://demo-shpo.resv.jp/direct_calendar.php?direct_id=2" class="btn -selConsul">この相談員さんに相談する</a>
            </div>
          </div>
        </article>
        <article class="consul-person">
          <div class="consul-person_img">
            <img src="<?= get_template_directory_uri(); ?>/images/guideline/consultant/kitagawa.jpg" alt="株式会社マザアス・北川順子">
          </div>
          <div class="consul-person_detail">
            <div class="consul-person_profile">
              <h3 class="consul-person_name"><ruby class="consul-person_name_ruby" data-ruby="きたがわ">北川</ruby> <ruby class="consul-person_name_ruby" data-ruby="じゅんこ">順子</ruby></h3>
              <p class="consul-person_com">株式会社マザアス</p>
              <p class="consul-person_part">介護福祉士 / 介護支援専門員 / 終活カウンセラー</p>
            </div>
            <dl class="consul-person_textBox">
              <dt class="consul-person_textBox_title">対応できる相談内容</dt>
              <dd class="consul-person_textBox_data">
                <ul class="consul-person_possible">
                  <li>在宅介護等に関わる相談全般</li>
                </ul>
              </dd>
            </dl>
            <dl class="consul-person_textBox js-consultantMessage">
              <dt class="consul-person_textBox_title is-spLink">相談員からのメッセージ</dt>
              <dd class="consul-person_textBox_data">
                <h4 class="consul-person_message_title">自ら『終の棲家』へ自宅を建て替えた実体験をもとに、高齢期の暮らし方ポイントをアドバイスさせていただきます！</h4>
                <p class="consul-person_message_text">医療機関でのデイケアや在宅介護サービス等、通算20年超の経験を経て、ご利用者ご家族の生活相談はじめ、住み慣れたご自宅で自分らしく暮らし続ける為のポイントを実体験と併せてご相談にのらせていただきます！</p>
              </dd>
            </dl>
            <div class="consul-person_btn">
              <a href="https://demo-shpo.resv.jp/direct_calendar.php?direct_id=3" class="btn -selConsul">この相談員さんに相談する</a>
            </div>
          </div>
        </article>
        <article class="consul-person">
          <div class="consul-person_img">
            <img src="<?= get_template_directory_uri(); ?>/images/guideline/consultant/hoshino.jpg" alt="神奈川ロイヤル株式会社・星野千鶴">
          </div>
          <div class="consul-person_detail">
            <div class="consul-person_profile">
              <h3 class="consul-person_name"><ruby class="consul-person_name_ruby" data-ruby="ほしの">星野</ruby> <ruby class="consul-person_name_ruby" data-ruby="ちづる">千鶴</ruby></h3>
              <p class="consul-person_com">神奈川ロイヤル株式会社</p>
              <p class="consul-person_part">老人ホーム / サービス付き高齢者向け住宅等のご紹介</p>
            </div>
            <dl class="consul-person_textBox">
              <dt class="consul-person_textBox_title">対応できる相談内容</dt>
              <dd class="consul-person_textBox_data">
                <ul class="consul-person_possible">
                  <li>老人ホーム、サービス付き高齢者向け住宅等のご紹介</li>
                </ul>
              </dd>
            </dl>
            <dl class="consul-person_textBox js-consultantMessage">
              <dt class="consul-person_textBox_title is-spLink">相談員からのメッセージ</dt>
              <dd class="consul-person_textBox_data">
                <h4 class="consul-person_message_title">首都圏（東京、神奈川、千葉、埼玉）を中心とした老人ホーム、サービス付き高齢者向け住宅等をご案内しております。パンフレットやネットにはのっていない現地の生の情報をお伝えいたします。</h4>
                <p class="consul-person_message_text">
                  私はお元気な時からお住み替えをしたいという方向けのサービス付き高齢者向け住宅や有料老人ホームをご案内しております。<br>
                  「できることはなるべく自分でしたい。」「介護付き有料老人ホームは安心だけ—っと自由に生活したい。」というご希望のもと、高齢者の方が介護にならないための生活の場をご提案させて頂きます。</p>
              </dd>
            </dl>
            <div class="consul-person_btn">
              <a href="https://demo-shpo.resv.jp/direct_calendar.php?direct_id=4" class="btn -selConsul">この相談員さんに相談する</a>
            </div>
          </div>
        </article>
      </div>
    </section>

  </div><!-- /.main-contents-->

  <?php get_sidebar(); ?>
  <!--============================================パンくずリスト（PCのみ）-->
  <ul class="breadcrumb -foot sp-only">
    <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
    <li><a href="<?= make_root_path(esc_url(home_url('/guideline/future-map')));?>">未来図の描き方</a></li>
    <li>総合相談窓口</li>
  </ul>
</main>
<?php get_footer(); ?>
