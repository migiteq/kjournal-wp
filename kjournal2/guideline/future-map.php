<!--?php /** * Template Name: 未来の描き方 */ ?-->
<?php get_header(); ?>
<?php
global $ARTICLE_TOP_URL;
?>
<main>
  <!--============================================パンくずリスト-->
  <ul class="breadcrumb">
    <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
    <li>未来図の描き方</li>
  </ul>

  <div class="main-contents">
    <div class="guide-cont -arrow">
      <!--============================================ページタイトル-->
      <div class="guide-cont_head">
        <h1 class="guide-cont_title -label"><span class="pc-only">これからの時間は</span>自分たちの人生のために。<br><span class="text -strong -line">暮らしの未来図</span>を描くことから<br class="sp-only">始めましょう。</h1>
        <p class="guide-cont_readText">子育ても一段落し、本当に充実した人生が始まるのはこれからです。将来を見据えて自分をリセットし、ちょっとのやる気を持てば、何にでもチャレンジできるのです。「後悔しない人生」を送るためにも、新たな夢や目標に向かって暮らしの未来を描いてみましょう。</p>
      </div>
      <!--============================================本文エリア-->
      <div class="guide-cont_body">
        <!--============================================STEP1 -->
        <section class="howto-step">
          <h2 class="howto-step_title">「住まいと住まい方」ビジョン表をつくる</h2>
          <p class="howto-step_text -illust">
            ビジョン表づくりは、暮らしの未来設計図をつくること。これからの暮らしの全体像をイメージするセカンドステージへの出発点です。<br>
            まず、やりたいことや夢を実現するために「住まいと住まい方」ビジョン表をつくって将来の暮らし方を”見える化”してみましょう。<br>
            <span class="howto-step_btn"><a href="https://www.satsuki-jutaku.jp/journal/article/p=984#chapter_2" class="btn -subLink">ビジョン表のダウンロード（PDF）<span class="pc-only">はこちら</span></a></span>
          </p>

          <div class="howto-point">
            <h3 class="howto-point_title">書き方のPOINT！</h3>
            <div class="howto-point_body">
              <dl class="howto-point_point">
                <dt class="howto-point_point_title">● 年表、家族の名前を記入</dt>
                <dd class="howto-point_point_text">西暦の欄の始まりに、現時点の年を入れる。（あとは５年、１０年など節目だけでもOK）<br>家族の名前を記入。</dd>
              </dl>
              <dl class="howto-point_point">
                <dt class="howto-point_point_title">● イベントを記入</dt>
                <dd class="howto-point_point_text">家族それぞれの年齢とイベントを記入。やりたいことやイベントの時期が重なる時は、優先順位を決める、タイミングをずらすなどして調整する。</dd>
              </dl>
              <dl class="howto-point_point">
                <dt class="howto-point_point_title">● 予定が変わればすぐに書き直す</dt>
                <dd class="howto-point_point_text">計画やイベントが実現できるように、予定が変わったら書き直す。常に目標をはっきりさせておきます。</dd>
              </dl>
            </div>
          </div>
<!--          <p class="guide-cont_link"><a href="#">→「住まいと住まい方」ビジョン表のダウンロード（PDF）はこちら</a></p>-->
        </section>
        <!--============================================STEP2 -->
        <section class="howto-step">
          <h2 class="howto-step_title">現在の生活環境と住まいの健康度のチェックを実施</h2>
          <p class="howto-step_text">現在の生活環境の中で、ご家族が快適に暮らし続けられるか、生活環境を再確認してみましょう。</p>
          <p class="guide-cont_link"><a href="https://www.satsuki-jutaku.jp/journal/article/p=1091#chapter_1">→「現在の生活環境と住まいの健康度チェック」<span class="pc-only">はこちら</span></a></p>
        </section>
        <!--============================================STEP3 -->
        <section class="howto-step">
          <h2 class="howto-step_title">より将来を想定したチェックを実施</h2>
          <p class="howto-step_text">現在の住まいが、ご家族にとって安全・快適な環境になっているかどうか、ご家族と住まいとの相性を確認してみましょう。将来については親御さんをイメージするとチェックしやすいでしょう。</p>
          <p class="guide-cont_link"><a href="https://www.satsuki-jutaku.jp/journal/article/p=1091#chapter_2">→「より将来を想定したチェック」<span class="pc-only">はこちら</span></a></p>
        </section>
      </div>
    </div>

<!--    <div class="guide-arrowText">たとえば、今の住まいで暮らし続けたい場合は<br>どうしたらいいんだろう…</div>-->

    <!--============================================CVポイント-->
    <div class="guide-summary -arrow">
      <section class="guide-summary_optBox">
        <h2 class="guide-summary_subTitle">「８つの配慮項目」をおさえて、<br class="sp-only">今の健康を維持し健康寿命をのばしましょう！</h2>
        <p class="guide-summary_text">リフォームは自分の家のプランに合わせて、配慮項目のどれを採り入れるのか施工業者と相談し、しっかり考えて依頼しましょう！</p>
        <p class="guide-summary_btn"><a class="btn -subLink" href="<?= make_root_path(esc_url(home_url('/guideline/considerations')));?>">「８つの配慮事項」について<span class="pc-only">詳しくはこちら</span></a></p>
      </section>
      <section class="guide-summary_mainBox -summary">
        <h2 class="guide-summary_title">住まいの改修や住み替えの<br class="sp-only">大切さはわかった...<br>でも、いつ決めたらいいの...？</h2>
        <div class="guide-summary_btn">
          <a class="btn -mainLink" href="<?= make_root_path(esc_url(home_url('/guideline/summary')));?>">将来の暮らしに備えるポイントを<br class="sp-only">確認しましょう！</a>
        </div>
      </section>
    </div>


  </div><!-- /.main-contents-->

  <?php get_sidebar(); ?>
  <!--============================================パンくずリスト（PCのみ）-->
  <ul class="breadcrumb -foot sp-only">
    <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
    <li>未来図の描き方</li>
  </ul>
</main>
<?php get_footer(); ?>
