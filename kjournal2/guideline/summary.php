<!--?php /** * Template Name: リフォームで目指す４つの安心 */ ?-->
<?php get_header(); ?>
<?php
global $ARTICLE_TOP_URL;
?>
<main>
  <!--============================================パンくずリスト-->
  <ul class="breadcrumb">
    <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
    <li><a href="<?= make_root_path(esc_url(home_url('/guideline/future-map')));?>">未来図の描き方</a></li>
    <li>リフォームで目指す４つの安心</li>
  </ul>

  <div class="main-contents">
    <div class="guide-cont">
      <!--============================================ページタイトル-->
      <div class="guide-cont_head">
        <h1 class="guide-cont_title">リフォームの大切さがわかったら<span class="text -strong -line">早めの備え</span>が必要です。<br><span class="text -strong">健康で元気な今のうちに</span>決断を！</h1>
        <p class="guide-cont_readText -summary">
          早い時期にリフォームをして住まいの環境を改善できれば、安全・快適で豊かな住生活と健康維持が図れます。万一、介護が必要になった場合も軽微な対応で暮らしが継続しやすく、介護や経済的な負担も軽減されます。<br>
          <span class="pc-only">元気な今も、やがて加齢が進む将来も、早めに決断して悠々自適なセカンドライフを創造してみませんか。</span>
        </p>
      </div>
      <!--============================================本文エリア-->
      <div class="guide-cont_body">
        <section class="summary-merit">
          <h2 class="summary-merit_title"><span>早めにリフォームすると、<br class="sp-only">こんなにメリットが！！</span></h2>
          <div class="summary-merit_list">
            <dl class="summary-merit_merit">
              <dt class="summary-merit_merit_title">光熱費を<br class="pc-only">節約できます</dt>
              <dd class="summary-merit_merit_text">ガス・水道・電気料金を節約できます。また、最新の設備機器への取り替えで、体がラクになり自分のために使える時間も増えます。</dd>
            </dl>
            <dl class="summary-merit_merit">
              <dt class="summary-merit_merit_title">日常生活が<br class="pc-only">より健康・安全に</dt>
              <dd class="summary-merit_merit_text">早くから身体への負担が少ない生活ができます。また、医療・介護費用の軽減や健康的な生活を送ることができます。</dd>
            </dl>
            <dl class="summary-merit_merit">
              <dt class="summary-merit_merit_title">セカンドライフが<br>より楽しく・快適に</dt>
              <dd class="summary-merit_merit_text">趣味や人とのコミュニケーションを楽しめる住まいにすれば、日々の暮らしが充実します。また、快適で豊かな暮らしを長く送ることができます。</dd>
            </dl>
          </div>
          <p class="guide-cont_notes">※ 改修に関しては<a href="<?= make_root_path(esc_url(home_url('/guideline/considerations')));?>">「８つの配慮事項」</a>を知るとより安心・安全な住まいを実現できます。</p>
        </section>

        <section class="summary-cont">
          <h2 class="summary-cont_title">ガイドラインが目指す４つの安心<span class="pc-only">のイメージ</span></h2>
          <div class="summary-cont_body">
            <p class="summary-cont_text">本ガイドラインでは改修する際に配慮すべきポイントについて8つの項目を提案しています。この配慮項目に基づく改修を実施することにより「高齢期の健康で快適な暮らしのための住まい」として、４つの安心の実現をめざします。</p>
            <div class="summary-cont_safeList">
              <dl class="summary-cont_safeImage -health">
                <dt class="summary-cont_safeImage_title">長く健康に暮らせる「住まい」</dt>
                <dd class="summary-cont_safeImage_text">安全・安心で、身体的・経済的な負担が少なくなり、外出や家事などが便利に</dd>
              </dl>
              <dl class="summary-cont_safeImage -independence">
                <dt class="summary-cont_safeImage_title">自立して自分らしく暮らせる「住まい」</dt>
                <dd class="summary-cont_safeImage_text">外出、趣味、交流を楽しむなど豊かな高齢期のライフスタイルに応じた空間の確保</dd>
              </dl>
              <dl class="summary-cont_safeImage -future">
                <dt class="summary-cont_safeImage_title">介護期になっても暮らせる「住まい」</dt>
                <dd class="summary-cont_safeImage_text">手すりの設置や福祉用具の使用など軽微な対応で暮らしを継続</dd>
              </dl>
              <dl class="summary-cont_safeImage -care">
                <dt class="summary-cont_safeImage_title">次世代に継承できる良質な「住まい」</dt>
                <dd class="summary-cont_safeImage_text">住まいの長寿命化に対応し、子どもやお孫さんにとっても住みやすく</dd>
              </dl>
            </div>

          </div>

        </section>
      </div>
    </div>


    <!--============================================CVポイント-->
    <div class="guide-summary -back">
      <section class="guide-summary_optBox">
                <p class="guide-summary_text">
                  <a class="btn -subLink" href="<?= $ARTICLE_TOP_URL;?>">トップページへ戻る</a>
                </p>
      </section>
<!--      <section class="guide-summary_mainBox -cv">-->
<!--        <h2 class="guide-summary_title">住まいの相談はこちらの窓口へ</h2>-->
<!--        <div class="guide-summary_btn">-->
<!--          <a class="btn -mainLink" href="#">総合相談の窓口一覧はこちら</a>-->
<!--        </div>-->
<!--        <p class="guide-summary_text"><a class="" href="--><?//= $ARTICLE_TOP_URL;?><!--">トップページへ戻る</a></p>-->
<!--      </section>-->
    </div>


  </div><!-- /.main-contents-->

  <?php get_sidebar(); ?>
  <!--============================================パンくずリスト（PCのみ）-->
  <ul class="breadcrumb -foot sp-only">
    <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
    <li><a href="<?= make_root_path(esc_url(home_url('/guideline/future-map')));?>">未来図の描き方</a></li>
    <li>リフォームで目指す４つの安心</li>
  </ul>
</main>
<?php get_footer(); ?>
