<?php
global $ARTICLE_TOP_URL;
$satsuki_url = "https://www.satsuki-jutaku.jp/";
$seo = init_tdk();
?>
<!doctype html>
<html lang="ja">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128665271-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-128665271-1');
  </script>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title><?= $seo['title'];?></title>
  <meta name="description" content="<?= $seo['desc'];?>"/>
  <meta name="keywords" content="<?= $seo['keyword'];?>"/>
  <?php if(is_home()): ?>
    <meta property="og:type" content="website" />
  <?php else:?>
    <meta property="og:type" content="article" />
  <?php endif;?>
  <meta property="og:title" content="<?= $seo['title'];?>" />
  <meta property="og:description" content="<?= $seo['desc'];?>" />
  <meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
  <?php if(is_single()): ?>
    <meta property="og:image" content="<?= CFS()->get('img_mv');?>" />
  <?php else:?>
    <meta property="og:image" content="<?= get_template_directory_uri();?>/screenshot.png" />
  <?php endif;?>
  <meta name="twitter:card" content="summary" />
  <?php if(is_tag() || is_search() || is_404() || is_page('allposts')): ?>
  <meta name="robots" content="noindex">
  <?php endif;?>
  <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri(); ?>/css/style.css?20201105">
  <base href="https://www.satsuki-jutaku.jp/journal/">
</head>

<body>
  <header id="header-wrapper">
    <!-- /⬇︎SP用ボタン -->
    <div class="sp-only btn -sp_header js-nav_toggle">
      <div class="header-btn-sp-inner">
        <div>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
    <!-- /⬆︎SP用ボタン -->
    <!-- /⬇︎既存header -->
    <div id="header">
      <div id="header-logo">
        <a href="index.php"><img src="<?php bloginfo('template_directory'); ?>/images/header-logo.png" width="362" height="67" alt="高齢者住宅ジャーナル"></a>
      </div>
      <div id="header-nav" class="pc-only">
        <ul style="text-align:right;">
          <li><a href="<?= $satsuki_url;?>inquiry.html">お問い合わせ</a></li>
          <li><a href="<?= $satsuki_url;?>link.html">リンク集</a></li>
          <li><a href="<?= $satsuki_url;?>sitemap.html">サイトマップ</a></li>
        </ul>

      </div>
      <div id="global-nav" class="clearfix pc-only">
        <ul>
          <li class="menu_1"><a href="<?= $satsuki_url;?>">ホーム</a></li>
          <li class="menu_2"><a href="<?= $satsuki_url;?>system.html">制度について</a></li>
          <li class="menu_3"><a href="<?= $satsuki_url;?>search/index.php">サービス付き高齢者向け住宅を探す</a></li>
          <li class="menu_4"><a href="<?= $satsuki_url;?>journal/" class="current">住まいと暮らしについて知る</a></li>
          <li class="menu_5"><a href="<?= $satsuki_url;?>apply.html">事業者の方へ</a></li>
          <li class="menu_6"><a href="<?= $satsuki_url;?>local_government.html">地方公共団体の方へ</a></li>
          <li class="menu_7"><a href="<?= $satsuki_url;?>news/list_all.html">お知らせ</a></li>
          <li class="menu_8"><a href="<?= $satsuki_url;?>faq.php">よくあるご質問</a></li>
        </ul>
      </div>
    </div>
    <!-- /⬆︎既存header -->

    <nav class="gnav pc-only">
      <ul>
        <?php
        //INIT::全カテゴリ情報取得
        $args = array('hide_empty'=>0,'orderby'=>'id');
        $categories = get_categories($args);
        foreach($categories as $category) :
          //INIT::カテゴリリンク
          $cat_link = make_root_path(get_category_link($category->term_id));
          if($category->slug==='live') $category->name = 'これからの住まいと暮らし';
          ?>
          <li class="">
            <a href="<?= $cat_link; ?>"><?= $category->name; ?></a>
          </li>
        <?php endforeach;?>
      </ul>
    </nav>
    <?php wp_head(); ?>
  </header>
