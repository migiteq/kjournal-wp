<?php
global $ARTICLE_TOP_URL;
//INIT::全カテゴリ情報取得
$args = array('hide_empty'=>0,'orderby'=>'id');
$categories = get_categories($args);
?>
<?php get_header(); ?>
  <main>
    <div class="main-contents">
        <!--============================================ガイドラインコンテンツ-->
        <section class="introGuideline">
            <h1 class="introGuideline_title">
                <picture>
                    <source media="(min-width: 481px)" srcset="<?= get_template_directory_uri(); ?>/images/guideline/title_text.svg">
                    <source media="(max-width: 480px)" srcset="<?= get_template_directory_uri(); ?>/images/guideline/title_text-sp.svg">
                    <img src="<?= get_template_directory_uri(); ?>/images/guideline/title_text.svg">
                </picture>
                <span class="introGuideline_subTitle">高齢期の健康で快適な暮らしのための<br class="sp-only">住まいの改修ガイドライン</span>
            </h1>
            <div class="introGuideline_body">
                <div class="introGuideline_problem">
                    <h2 class="introGuideline_problem_title">人生の折り返し点をすぎたら、<br class="pc-only">そろそろ、将来のこと考え始めてみませんか？</h2>
                    <p class="introGuideline_problem_body">「人生100年時代」と言われる現代、50歳を過ぎてもようやく折り返し点です。<br>今は元気でも、やがて加齢による体力低下が起きて今の住まい方にも変化が求められてきます。そろそろ将来の暮らし方、考えてみませんか。</p>
                </div>
                <div class="introGuideline_purpose">
                    <h2 class="introGuideline_purpose_title">
                        <picture>
                            <source media="(min-width: 481px)" srcset="<?= get_template_directory_uri(); ?>/images/guideline/purpose_title.svg">
                            <source media="(max-width: 480px)" srcset="<?= get_template_directory_uri(); ?>/images/guideline/purpose_title-sp.svg">
                            <img src="<?= get_template_directory_uri(); ?>/images/guideline/purpose_title.svg" alt="「高齢期の健康で快適な暮らしのための住まいの改修ガイドライン」で４つの安心を実現しましょう！">
                        </picture>
                    </h2>
                    <ul class="introGuideline_purpose_list">
                        <li class="introGuideline_purpose_item">長く健康に暮らせる</li>
                        <li class="introGuideline_purpose_item">自立して自分らしく<br>暮らせる</li>
                        <li class="introGuideline_purpose_item">介護期になっても<br>暮らせる</li>
                        <li class="introGuideline_purpose_item">次世代に継承できる</li>
                    </ul>
                </div>
              <div CLASS="introGuideline_link"><a class="btn -mainLink" href="<?= make_root_path(esc_url(home_url('/guideline/future-map')));?>"><span class="pc-only">さっそく、</span>将来の暮らしについて<br class="sp-only">備える方法を知る</a></div>

              <!--============================================ガイドライン固定記事表示エリア-->
              <?php
              //広告記事が１件もない場合はコンテンツを出さない
              $args= array('include'=>'739,984,987');
              $posts = get_posts($args);
              if (!empty($posts)):?>
                <article class="introGuideline_articles">
                  <h2 class="introGuideline_articles_title">「これからの住まいと暮らし」<span class="pc-only">について</span><br class="sp-only">のピックアップ記事</h2>
                  <div class="introGuideline_articles_body">
                    <ul class="list --dot">
                      <?php
                      foreach($posts as $post):
                        $data = [
                          'link'=>make_root_path($post->guid),
                          'title'=>text_ellipsis($post->post_title, 0, 60, "…", "UTF-8"),
                        ];
                        ?>
                        <li class="list_item"><a href="<?= $data['link']; ?>"><?= $data['title'];?></a></li>
                      <?php endforeach;?>
                    </ul>
                  </div>
                </article>
              <?php endif; ?>

            </div>
        </section>

      <!--============================================最新記事表示エリア（PR以外）-->
      <div class="l-cont -latest">
        <div class="l-cont_title">
          <h2 class="title -latest">最新記事<span class="pc-only">(<a href="<?= make_root_path(esc_url(home_url('/allposts')));?>">全ての最新記事を見る</a>)</span></h2>
        </div>
        <ul class="l-cont_list">
          <?php
          if (have_posts()) :
            $args= build_args(array('pr'=>0,'sort'=>'DESC','count'=>4));
            $posts = get_posts($args);
            foreach($posts as $post):
              //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
              if (has_post_thumbnail()){
                $thumbnail_id = get_post_thumbnail_id();
                $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
              } else {
                $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
              }
              //INIT::記事情報_カテゴリ
              $cat_post = get_the_category($post->ID)[0];
              if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
              $data = [
                  'pr'=> intval(get_post_meta($post->ID,'pr',true)),
                  'link'=>make_root_path($post->guid),
                  'title'=>text_ellipsis($post->post_title, 0, 60, "…", "UTF-8"),
                  'cat_name'=> $cat_post->cat_name,
                  'cat_slug'=>$cat_post->slug,
                  'content'=>array('img'=>$img_url)
              ];

              ?>
              <li>
                <a href="<?= $data['link']; ?>">
                  <article class="cassette -vertical">
                    <div class="cassette_img">
                      <?php if(!empty($data['content']['img'])): ?>
                        <img src="<?= $data['content']['img'];?>" width="175" height="100" class="object-fit_img">
                      <?php else: ?>
                        <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                      <?php endif; ?>
                    </div>
                    <div class="cassette_detail">
                      <div class="cassette_label">
                        <span class="label -full -cat -<?= $data['cat_slug']; ?>"><?= $data['cat_name'];?></span>
                      </div>
                      <div class="cassette_title"><?= $data['title'];?></div>
                    </div>
                  </article>
                </a>
              </li>
            <?php endforeach; else : ?>
            <li><?php _e('投稿はありません。'); ?></li>
          <?php endif; ?>
        </ul>
          <div class="sp-only l-cont_more"><a class="btn -more" href="<?= make_root_path(esc_url(home_url('/allposts')));?>">すべての最新記事を見る</a></div>
      </div>

      <!--============================================広告記事表示エリア-->
      <?php
      //広告記事が１件もない場合はコンテンツを出さない
      $args= build_args(array('pr'=>1));
      $flg = get_posts($args);
      if (!empty($flg)):?>
        <div class="l-cont -pr">
          <ul class="l-cont_list">
            <?php
            //取得条件::更新最新順、２件、PR記事のみ
            $args= build_args(array('count'=>2,'pr'=>1));
            $posts = get_posts($args);
            foreach($posts as $post):
              //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
              if (has_post_thumbnail()){
                $thumbnail_id = get_post_thumbnail_id();
                $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
              } else {
                $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
              }
              //INIT::記事情報_カテゴリ
              $cat_post = get_the_category($post->ID)[0];
              if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
              $data = [
                'pr'=> intval(get_post_meta($post->ID,'pr',true)),
                'link'=>make_root_path($post->guid),
                'title'=>text_ellipsis($post->post_title, 0, 74, "…", "UTF-8"),
                'cat_name'=> $cat_post->cat_name,
                'cat_slug'=>$cat_post->slug,
                'content'=>array('img'=>$img_url,)
              ];
              ?>
              <li>
                <a href="<?= $data['link']; ?>">
                  <article class="cassette -category">
                    <div class="cassette_img">
                      <?php if(!empty($data['content']['img'])): ?>
                        <img src="<?= $data['content']['img'];?>" width="80" height="80" class="object-fit_img">
                      <?php else: ?>
                        <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                      <?php endif; ?>
                    </div>
                    <div class="cassette_detail">
                      <div class="cassette_label">
                        <span class="label -cat -<?= $data['cat_slug'];?>"><?= $data['cat_name'];?></span>
                        <?php if($data['pr'] === 1): ?><span class="label -pr">PR</span><?php endif;?>
                      </div>
                      <div class="cassette_title"><?= $data['title']; ?></div>
                    </div>
                  </article>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div><!-- /.top-pr-area -->
      <?php endif; ?>
      <!--============================================カテゴリ毎の記事表示エリア-->
      <?php
      //ここからカテゴリ毎のコンテンツ生成
      $args = array('hide_empty'=>0,'orderby'=>'id');
      $categories = get_categories($args);
      foreach($categories as $category) :
        //INIT::カテゴリリンク
        $cat_link = make_root_path(get_category_link($category->term_id));
        if($category->count > 0):
        ?>
        <section class="l-cont">
          <div class="l-cont_title">
            <h2 class="title -chapter"><span><?= $category->cat_name; ?></span></h2>
          </div>
          <ul class="l-cont_list">
            <?php
            //ここからカテゴリ毎の記事生成
            $args= build_args(array('sort'=>'DESC','count'=>6,'cat'=>$category->term_id,));
            $posts = get_posts($args);
            foreach($posts as $post): setup_postdata($post);
              //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
              if (has_post_thumbnail()){
                $thumbnail_id = get_post_thumbnail_id();
                $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
              } else {
                $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
              }
              //INIT::記事情報_カテゴリ
              $cat_post = get_the_category($post->ID)[0];
              if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
              $data = [
                  'pr'=> intval(get_post_meta($post->ID,'pr',true)),
                  'link'=>make_root_path($post->guid),
                  'title'=>text_ellipsis($post->post_title, 0, 74, "…", "UTF-8"),
                  'cat_name'=> $cat_post->cat_name,
                  'cat_slug'=>$cat_post->slug,
                  'content'=>array('img'=>$img_url)
              ];
              ?>
              <li>
                <a href="<?= $data['link']; ?>">
                  <article class="cassette -category <?php if($data['pr'] === 1) echo 'is-pr'; ?>">
                    <div class="cassette_img">
                      <?php if(!empty($data['content']['img'])): ?>
                        <img src="<?= $data['content']['img'];?>" width="80" height="80" class="object-fit_img">
                      <?php else: ?>
                        <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                      <?php endif; ?>
                    </div>
                    <div class="cassette_detail">
                      <div class="cassette_label">
                        <span class="label -cat -<?= $data['cat_slug']; ?>"><?= $data['cat_name'];?></span>
                        <?php if($data['pr'] === 1): ?><span class="label -pr">PR</span><?php endif;?>
                      </div>
                      <div class="cassette_title"><?= $data['title']; ?></div>
                    </div>
                  </article>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
          <div class="l-cont_more"><a class="btn -more" href="<?= $cat_link; ?>">このカテゴリの記事一覧を見る</a></div>
        </section><!-- /.l-cont-->
      <?php endif;endforeach; ?>

    </div>

    <?php get_sidebar(); ?>
  </main>
<?php get_footer(); ?>
