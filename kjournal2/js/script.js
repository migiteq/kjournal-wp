$(function(){
  // ----------------------------- SPメニュー
  $('.js-nav_toggle').on("click",function(){
    $("#header-wrapper").toggleClass('is-show');
    $(".side-menu").toggleClass('is-show');
    $(".btn.-sp_header").toggleClass('is-show');
  });
  // ----------------------------- pagetopボタン表示
  var topBtn=$('#pageTop');
  topBtn.hide();
  $(window).on('scroll', function(){
    $(this).scrollTop() > 80 ? topBtn.fadeIn() : topBtn.fadeOut();
  });
  // ----------------------------- スムーススクロール
  $('a[href^="#"]').on('click',function(ev){
    ev.preventDefault();
    var speed = 400,
        href = $(this).attr('href'),
//          $target = $(href === "#" || href === "" ? 'html' : href),
        pos = href === "#" || href === "" ? 0 : $(href).offset().top - 80;
    $('body,html').animate({scrollTop: pos},speed);
    return false;
  });
  // ----------------------------- 相談窓口導線：相談員メッセージ
  var $consultantMessage = $('.js-consultantMessage');
  var winW = window.innerWidth;


  if($consultantMessage.length && winW <= 480){
    var $acTrg = $consultantMessage.find('.is-spLink');
    $acTrg.on('click',function(ev) {
      ev.preventDefault();
      console.log(winW);
      $(this).removeClass('is-spLink');
    })
  }
});