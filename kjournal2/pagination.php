<?php

//変数受け取り
$paged = get_query_var('paged');
$the_query = get_query_var('the_query');

//------------------------------INIT
if(is_page('allposts')){
    $currentPage = make_root_path(esc_url(home_url('/allposts')));
}else{
    $currentPage = (empty($_SERVER['HTTPS']) ? 'http://' : 'https://').$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
    $currentPage = strtok($currentPage, '?');
}
$pageNum = intval($the_query->max_num_pages);
//件数表示用
$allNum = $the_query->found_posts;
$minNum = (($paged - 1) * 10) + 1;
$maxNum = ($allNum > $paged * 10)? $paged * 10 : $allNum;

$baseURL = $currentPage;
$r = paginate_links(array(
    'base' => $baseURL.'%_%',
    'format' => '?page=%#%',
    'prev_next'=>false,
    'type'=>'array',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $pageNum
));

?>
<div class="l-cont_aside">
    <div class="listInfo">
        <div class="listInfo_count">（<?= $minNum.'-'.$maxNum.'件/全'.$allNum.'件'?>）</div>
        <div class="listInfo_func">
            <ul class="pagination">
                <li class="pagination_func <?php if($paged === 1)echo 'is-disabled';?>">
                    <?php if($paged === 1): ?>
                        <span>前頁</span>
                    <?php else: ?>
                        <a href="<?= $baseURL."?page=".($paged - 1) ;?>">前頁</a>
                    <?php endif;?>
                </li>
                <li>
                    <ul class="pagination_num">
                        <?php if(!$r):?>
                            <li class="is-active"><span>1</span></li>
                        <?php else:?>
                            <?php for ($i=0 ; $i <= $pageNum; $i++):?>
                                <li <?php if($i+1 === $paged)echo 'class="is-active"';?>><?= $r[$i];?></li>
                            <?php endfor;?>
                        <?php endif;?>
                    </ul>
                </li>
                <li class="pagination_func <?php if($pageNum === $paged)echo 'is-disabled';?>">
                    <?php if($paged === $pageNum): ?>
                        <span>次頁</span>
                    <?php else: ?>
                        <a href="<?= $baseURL."?page=".($paged + 1) ;?>">次頁</a>
                    <?php endif;?>
                </li>
            </ul>
        </div>
    </div>
</div>
