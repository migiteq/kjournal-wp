<?php
$the_query = get_query_var('the_query');
$list_kind = get_query_var('list_kind');
?>

<ul class="l-cont_list">
    <?php while ($the_query->have_posts()) : $the_query->the_post();
            //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
            if (has_post_thumbnail()){
                $thumbnail_id = get_post_thumbnail_id();
                $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
            } else {
                $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
            }
        //INIT::記事情報_カテゴリ
        $cat_post = get_the_category($post->ID)[0];
        if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
        $data = [
            'pr'=> intval(get_post_meta($post->ID,'pr',true)),
            'link'=>make_root_path($post->guid),
            'title'=>text_ellipsis($post->post_title, 0, 74, "…", "UTF-8"),
            'read_text'=>text_ellipsis(get_post_meta($post->ID,'text_mv',true),0,60, "…", "UTF-8"),
            'cat_name'=> $cat_post->cat_name,
            'cat_slug'=>$cat_post->slug,
            'content'=>array('img'=>$img_url)
        ];
        ?>
        <li>
            <a href="<?= $data['link']; ?>">
                <article class="cassette -archive <?php if($data['pr'] === 1) echo 'is-pr'; ?>">
                    <div class="cassette_img">
                        <?php if(!empty($data['content']['img'])): ?>
                            <img src="<?= $data['content']['img'];?>" width="110" height="110" class="object-fit_img">
                        <?php else: ?>
                            <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                        <?php endif; ?>
                    </div>
                    <div class="cassette_detail">
                        <div class="cassette_title">
                            <?php if($list_kind !== "category"):?>
                                <span class="label -cat -<?= $data['cat_slug']; ?>"><?= $data['cat_name'];?></span>
                            <?php endif;?>
                            <?= $data['title']; ?>
                        </div>
                        <p class="cassette_text"><?= $data['read_text']; ?></p>
                        <?php
                        //タグ情報の生成
                        $posttags = get_the_tags();
                        if ($posttags || $data['pr'] === 1):?>
                            <div class="cassette_label">
                                <?php if(!empty($posttags)):foreach($posttags as $tag):?>
                                    <span class="label -tag"><?= $tag->name;?></span>
                                <?php endforeach;endif;?>
                                <?php if($data['pr'] === 1): ?><span class="label -pr">PR</span><?php endif;?>
                            </div>
                        <?php endif;?>
                    </div>
                </article>
            </a>
        </li>
        <?php endwhile; ?>
</ul>
