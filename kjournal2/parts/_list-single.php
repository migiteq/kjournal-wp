<?php
$list_kind = get_query_var('list_kind');
$post_cat = get_query_var('post_cat');
$this_id = get_query_var('this_id');
//$argの生成
switch ($list_kind) {
    case "recommend":
        //取得条件::別カテゴリの記事、4件、ランダム
        $args= build_args(array('count'=>4,'cat'=>-$post_cat->term_id));
        break;
    case "related":
        //取得条件::当該カテゴリのみ、4件、ランダム
        $args= build_args(array('count'=>4,'cat'=>$post_cat->term_id,'exclude'=>$this_id));
        break;
}
?>

<ul class="l-cont_list">
    <?php
    if (have_posts()) :
        $posts = get_posts($args);
        foreach($posts as $post):
            //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
            if (has_post_thumbnail()){
                $thumbnail_id = get_post_thumbnail_id();
                $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
            } else {
                $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
            }
            //INIT::記事情報_カテゴリ
            $cat_post = get_the_category($post->ID)[0];
            if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
            $data = [
                'pr'=> intval(get_post_meta($post->ID,'pr',true)),
                'link'=>make_root_path($post->guid),
                'title'=>text_ellipsis($post->post_title, 0, 60, "…", "UTF-8"),
                'cat_name'=> $cat_post->cat_name,
                'cat_slug'=>$cat_post->slug,
                'content'=>array('img'=>$img_url)
            ];
            ?>

            <li>
                <a href="<?= $data['link']; ?>">
                    <article class="cassette -vertical">
                        <div class="cassette_img">
                            <?php if(!empty($data['content']['img'])): ?>
                                <img src="<?= $data['content']['img'];?>" width="175" height="100" class="object-fit_img">
                            <?php else: ?>
                                <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                            <?php endif; ?>
                        </div>
                        <div class="cassette_detail">
                            <div class="cassette_label">
                                <span class="label -full -cat -<?= $data['cat_slug']; ?>"><?= $data['cat_name'];?></span>
                            </div>
                            <div class="cassette_title"><?= $data['title'];?></div>
                        </div>
                    </article>
                </a>
            </li>

        <?php endforeach; else : ?>
        <li><?php _e('投稿はありません。'); ?></li>
    <?php endif; ?>
</ul>
