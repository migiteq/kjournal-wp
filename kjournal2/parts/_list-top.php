<?php
$list_kind = get_query_var('list_kind');
//$argの生成
switch ($list_kind) {
    case "latest":
        //取得条件::更新最新順、4件、PR記事除く
        $args= build_args(array('pr'=>0,'sort'=>'DESC','count'=>4));
        break;
    case "pr":
        //取得条件::ランダム、２件、PR記事のみ
        $args= build_args(array('count'=>2,'pr'=>1));
        break;
    case "pr-cat":
        $cat = get_query_var('cat');
        //取得条件::更新最新順、２件、PR記事のみ、当該カテゴリのみ
        $args= build_args(array('sort'=>'DESC','pr'=>1,'count'=>2,'cat'=>$cat->term_id));
    case "pr-tag":
        $tag = get_query_var('tag');
        //取得条件::更新最新順、２件、PR記事のみ、当該タグのみ
        $args= build_args(array('sort'=>'DESC','pr'=>1,'count'=>2,'tag'=>$tag->slug));
    case "cat":
        $category = get_query_var('category');
        //ここからカテゴリ毎の記事生成
        $args= build_args(array('sort'=>'DESC','count'=>6,'cat'=>$category->term_id,));
        break;
}
?>

<ul class="l-cont_list">
    <?php
    echo $cat->term_id;
    if (have_posts()) :
        $posts = get_posts($args);
        foreach($posts as $post):
            //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
            if (has_post_thumbnail()){
                $thumbnail_id = get_post_thumbnail_id();
                $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
            } else {
                $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
            }
            //INIT::記事情報_カテゴリ
            $cat_post = get_the_category($post->ID)[0];
            if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
            ?>

            <?php if($list_kind === "latest"):
            $data = [
                'pr'=> intval(get_post_meta($post->ID,'pr',true)),
                'link'=>make_root_path($post->guid),
                'title'=>text_ellipsis($post->post_title, 0, 60, "…", "UTF-8"),
                'cat_name'=> $cat_post->cat_name,
                'cat_slug'=>$cat_post->slug,
                'content'=>array('img'=>$img_url)
            ]; ?>
            <li>
                <a href="<?= $data['link']; ?>">
                    <article class="cassette -vertical">
                        <div class="cassette_img">
                            <?php if(!empty($data['content']['img'])): ?>
                                <img src="<?= $data['content']['img'];?>" width="175" height="100" class="object-fit_img">
                            <?php else: ?>
                                <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                            <?php endif; ?>
                        </div>
                        <div class="cassette_detail">
                            <div class="cassette_label">
                                <span class="label -full -cat -<?= $data['cat_slug']; ?>"><?= $data['cat_name'];?></span>
                            </div>
                            <div class="cassette_title"><?= $data['title'];?></div>
                        </div>
                    </article>
                </a>
            </li>
        <?php elseif($list_kind === "pr" || "pr-tag" || "pr-cat" || "cat")://横並びカセット
            $data = [
                'pr'=> intval(get_post_meta($post->ID,'pr',true)),
                'link'=>make_root_path($post->guid),
                'title'=>text_ellipsis($post->post_title, 0, 74, "…", "UTF-8"),
                'cat_name'=> $cat_post->cat_name,
                'cat_slug'=>$cat_post->slug,
                'content'=>array('img'=>$img_url,)
            ]; ?>
            <li>
                <a href="<?= $data['link']; ?>">
                    <article class="cassette<?php if($list_kind ==="cat" && $data['pr'] === 1) echo ' is-pr';?>" >
                        <div class="cassette_img">
                            <?php if(!empty($data['content']['img'])): ?>
                                <img src="<?= $data['content']['img'];?>" width="80" height="80" class="object-fit_img">
                            <?php else: ?>
                                <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                            <?php endif; ?>
                        </div>
                        <div class="cassette_detail">
                            <div class="cassette_label">
                                <span class="label -cat -<?= $data['cat_slug'];?>"><?= $data['cat_name'];?></span>
                                <?php if($data['pr'] === 1): ?><span class="label -pr">PR</span><?php endif;?>
                            </div>
                            <div class="cassette_title"><?= $data['title']; ?></div>
                        </div>
                    </article>
                </a>
            </li>
        <?php endif;?>

        <?php endforeach; else : ?>
        <li><?php _e('投稿はありません。'); ?></li>
    <?php endif; ?>
</ul>
