<?php get_header(); ?>

<?php
global $ARTICLE_TOP_URL;
$paged = intval((get_query_var('paged'))?get_query_var('paged'):1);
?>

  <main>
    <!--============================================パンくずリスト-->
    <ul class="breadcrumb">
      <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
      <li class="pc-only">「<?= $s; ?>」の検索結果</li>
    </ul>

    <div class="main-contents">
      <!--============================================ページタイトル-->
      <div class="intro">
        <div class="intro_title">
          <h1 class="title -page">「<?= $s; ?>」の検索結果</h1>
        </div>
      </div>


      <!--============================================検索結果表示エリア-->

      <?php
      //------------------------------記事取得用INIT
      $args = array(
          'orderby'=> 'modified',
          'post_type' => 'post',
          'posts_per_page' => 10,
          'paged' => $paged,//現在のページ番号の指定
        's'=>$s
      );
      $the_query = new WP_Query($args);
      //変数をテンプレートファイルで使用できるようにする
      set_query_var( 'the_query', $the_query );
      set_query_var( 'paged', $paged);
      ?>
      <div class="l-cont -archive">
        <?php if ($the_query->have_posts()) : ?>

          <?php get_template_part('pagination');//ページネーション ?>
          <ul class="l-cont_list">
            <?php
            while ($the_query->have_posts()) : $the_query->the_post();?>

              <?php
              //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
              if (has_post_thumbnail()){
                $thumbnail_id = get_post_thumbnail_id();
                $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
              } else {
                $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
              }
              $post_cat = get_the_category($post->ID)[0];
              if($post_cat->slug==='live') $post_cat->cat_name = '住まいと暮らし';
              $data = [
                  'pr'=> intval(get_post_meta($post->ID,'pr',true)),
                  'link'=>make_root_path($post->guid),
                  'title'=>text_ellipsis($post->post_title, 0, 74, "…", "UTF-8"),
                  'read_text'=>text_ellipsis(get_post_meta($post->ID,'text_mv',true),0,60, "…", "UTF-8"),
                  'cat_name'=> $post_cat->cat_name,
                  'cat_slug'=>$post_cat->slug,
                  'content'=>array('img'=>$img_url)
              ]; ?>
              <li>
                <a href="<?= $data['link']; ?>">
                  <article class="cassette -archive <?php if($data['pr'] === 1) echo 'is-pr'; ?>">
                    <div class="cassette_img">
                      <?php if(!empty($data['content']['img'])): ?>
                        <img src="<?= $data['content']['img'];?>" width="110" height="110" class="object-fit_img">
                      <?php else: ?>
                        <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                      <?php endif; ?>
                    </div>
                    <div class="cassette_detail">
                      <div class="cassette_title">
                        <span class="label -cat -<?= $data['cat_slug']; ?>"><?= $data['cat_name'];?></span>
                        <?= $data['title']; ?>
                      </div>
                      <p class="cassette_text"><?= $data['read_text']; ?></p>
                      <?php
                      //タグ情報の生成
                      $posttags = get_the_tags();
                      if ($posttags || $data['pr'] === 1):?>
                        <div class="cassette_label">
                          <?php if(!empty($posttags)):foreach($posttags as $tag):?>
                            <span class="label -tag"><?= $tag->name;?></span>
                          <?php endforeach;endif;?>
                          <?php if($data['pr'] === 1): ?><span class="label -pr">PR</span><?php endif;?>
                        </div>
                      <?php endif;?>
                    </div>
                  </article>
                </a>
              </li>

            <?php endwhile;
            ?>
          </ul>

          <?php get_template_part('pagination');//ページネーション ?>

        <?php else:?>
          <div class="l-cont_text"><p class="no-post"><?php _e('「'.$s.'」に一致する情報は見つかりませんでした。'); ?></p></div>
          <div class="l-cont_back">
            <a href="<?= $ARTICLE_TOP_URL;?>" class="btn -back">高齢者住宅ジャーナルTOPに戻る</a>
          </div>
        <?php endif; wp_reset_postdata(); ?>
      </div>

    </div><!-- /.main-contents-->

    <?php get_sidebar(); ?>
    <!--============================================パンくずリスト（PCのみ）-->
    <ul class="breadcrumb -foot sp-only">
      <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
    </ul>
  </main>
<?php get_footer(); ?>