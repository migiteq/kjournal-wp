<?php
global $ARTICLE_TOP_URL;
$satsuki_url = "https://www.satsuki-jutaku.jp/";
?>
<aside class="side-menu">
  <section class="l-cont -side">
    <div class="l-cont_title">
      <h2 class="title -sideMain"><span class="pc-only">お役立ち記事</span><span class="sp-only">もくじ</span></h2>
    </div>
    <div class="l-cont_sub">
      <a class="btn -goHome" href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a>
    </div>

    <!--============================================検索窓-->
    <section>
      <h3 class="title -sideSub">単語で記事を探す</h3>
      <div class="l-cont_sub"><?php get_search_form(); ?></div>
    </section>
    <!--============================================カテゴリ一覧リンク-->
    <section>
      <h3 class="title -sideSub">カテゴリで記事を探す</h3>
      <div class="l-cont_sub">
        <ul class="sideNav">
          <?php
          //INIT::カテゴリ情報取得
          $args = array('hide_empty'=>0,'orderby'=>'id');
          $categories = get_categories($args);
          foreach($categories as $category) :
            //INIT::カテゴリリンク
            $cat_link = make_root_path(get_category_link($category->term_id));
            if($category->slug==='live') $category->name = 'これからの住まいと暮らし';
            ?>
            <li class="sideNav_item -<?= $category->slug; ?>">
              <a href="<?= $cat_link; ?>"><?= $category->name; ?></a>
            </li>
          <?php endforeach;?>
        </ul>
      </div>
    </section>
    <!--============================================特集バナー-->
    <section>
      <h3 class="title -sideSub">特集</h3>
      <div class="l-cont_sub">
        <div class="sideBanner">
          <a href="<?= make_root_path(esc_url(home_url('/guideline/future-map')));?>">
            <picture>
              <source media="(min-width: 481px)" srcset="<?= get_template_directory_uri(); ?>/images/banners/guideline_banner-pc-top.png">
              <source media="(max-width: 480px)" srcset="<?= get_template_directory_uri(); ?>/images/banners/guideline_banner-sp-top.png">
              <img src="<?= get_template_directory_uri(); ?>/images/banners/guideline_banner-pc-top.png">
            </picture>
          </a>
        </div>
        <?php if(is_page('consultation')): ?>
        <div class="sideBanner">
          <a href="<?= make_root_path(esc_url(home_url('/guideline/consultation')));?>">
            <picture>
              <source media="(min-width: 481px)" srcset="<?= get_template_directory_uri(); ?>/images/banners/consul_banner-pc.png">
              <source media="(max-width: 480px)" srcset="<?= get_template_directory_uri(); ?>/images/banners/consul_banner-sp.png">
              <img src="<?= get_template_directory_uri(); ?>/images/banners/consul_banner-pc.png">
            </picture>
          </a>
        </div>
        <?php endif;?>
      </div>
    </section>
    <!--============================================最新記事一覧リンク（PCのみ）-->
    <section class="pc-only">
      <h3 class="title -sideSub">最新記事</h3>
      <ul class="l-cont_list">
        <?php

        $args = build_args(array('pr'=>0,'sort'=>'DESC','count'=>4));
        $the_query = new WP_Query($args);

        if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post();
          //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
          if (has_post_thumbnail()){
            $thumbnail_id = get_post_thumbnail_id();
            $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
          } else {
            $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
          }
        //INIT::記事情報_カテゴリ
        $cat_post = get_the_category($post->ID)[0];
          if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
        $data = [
            'pr'=> intval(get_post_meta($post->ID,'pr',true)),
            'link'=>make_root_path($post->guid),
            'title'=>text_ellipsis($post->post_title, 0, 32, "…", "UTF-8"),
            'cat_name'=> $cat_post->cat_name,
            'cat_slug'=>$cat_post->slug,
            'content'=>array('img'=>$img_url)
        ];
        ?>
            <li>
              <a href="<?= $data['link']; ?>">
                <article class="cassette -side">
                  <div class="cassette_img">
                    <?php if(!empty($data['content']['img'])): ?>
                      <img src="<?= $data['content']['img'];?>" width="64" height="64" class="object-fit_img">
                    <?php else: ?>
                      <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                    <?php endif; ?>
                  </div>
                  <div class="cassette_title">
                    <div class="cassette_label">
                      <span class="label -side -cat -<?= $data['cat_slug'];?>"><?= $data['cat_name'];?></span>
                    </div>
                    <?= $data['title']; ?></div>
                </article>
              </a>
            </li>
        <?php endwhile; else : ?>
          <li><?php _e('投稿はありません。'); ?></li>
        <?php endif; ?>
      </ul>
      <div class="l-cont_more"><a class="btn -more" href="<?= make_root_path(esc_url(home_url('/allposts')));?>">すべての最新記事を見る</a></div>
    </section>

    <!--============================================人気の記事一覧リンク（PCのみ、記事詳細ページのみ）-->
    <section class="pc-only">
      <h3 class="title -sideSub">人気の記事ランキング</h3>
      <ul class="l-cont_list">
        <?php

        $args = build_args(array('ranking'=>true,'sort'=>'DESC','count'=>5));
        $the_query = new WP_Query($args);

        if ($the_query->have_posts()): while ($the_query->have_posts()): $the_query->the_post();
          //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
          if (has_post_thumbnail()){
            $thumbnail_id = get_post_thumbnail_id();
            $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
          } else {
            $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
          }
          //INIT::記事情報_カテゴリ
          $cat_post = get_the_category($post->ID)[0];
          if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
          $data = [
              'pr'=> intval(get_post_meta($post->ID,'pr',true)),
              'link'=>make_root_path($post->guid),
              'title'=>text_ellipsis($post->post_title, 0, 32, "…", "UTF-8"),
              'cat_name'=> $cat_post->cat_name,
              'cat_slug'=>$cat_post->slug,
              'content'=>array('img'=>$img_url)
          ];
          ?>
          <li>
            <a href="<?= $data['link']; ?>">
              <article class="cassette -side -ranking">
                <div class="cassette_img">
                  <?php if(!empty($data['content']['img'])): ?>
                    <img src="<?= $data['content']['img'];?>" width="64" height="64" class="object-fit_img">
                  <?php else: ?>
                    <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                  <?php endif; ?>
                </div>
                <div class="cassette_title">
                  <div class="cassette_label">
                    <span class="label -side -cat -<?= $data['cat_slug'];?>"><?= $data['cat_name'];?></span>
                  </div>
                  <?= $data['title']; ?>
                </div>
              </article>
            </a>
          </li>
        <?php endwhile; else : ?>
          <li><?php _e('投稿はありません。'); ?></li>
        <?php endif; ?>
      </ul>
    </section>
<!--    --><?php //endif;?>


    <!--============================================グローバルリンク（SPのみ）-->
    <section class="sp-only">
      <h3 class="title -sideSub">他のコンテンツを見る</h3>
      <div class="l-cont_sub">
        <ul class="sideNav -other">
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>">情報提供システム（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>system.html">制度について（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>search/index.php">サービス付き高齢者向け住宅を探す（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>apply.html">事業者の方へ（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>local_government.html">地方公共団体の方へ（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>news/list_all.html">お知らせ（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>faq.php">よくある質問（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>policy.html">プライバシーポリシー（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>inquiry.html">お問い合わせ（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>link.html">リンク集（PC）</a></li>
          <li class="sideNav_item"><a href="<?= $satsuki_url;?>sitemap.html">サイトマップ（PC）</a></li>
        </ul>
      </div>
    </section>
    <!--============================================関連リンク（SPのみ）-->
    <section class="sp-only">
      <h3 class="title -sideSub">関連リンク</h3>
      <div class="l-cont_sub">
        <ul class="sideNav -other">
          <li class="sideNav_item"><a href="http://www.mlit.go.jp/" target="_blank">国土交通省</a></li>
          <li class="sideNav_item"><a href="http://www.mhlw.go.jp/" target="_blank">厚生労働省</a></li>
          <li class="sideNav_item"><a href="http://www.shpo.or.jp/" target="_blank">一般社団法人高齢者住宅協会</a></li>
        </ul>
      </div>
    </section>
      <!--============================================バナーエリア（バナー届き次第実装）-->
<!--      <article class="sideBanner">-->
<!--          <div class="l-cont_sub">-->
<!--              <picture>-->
<!--                  <source media="(min-width: 481px)" srcset="http://placehold.jp/300x250.png">-->
<!--                  <source media="(max-width: 480px)" srcset="http://placehold.jp/640x200.png">-->
<!--                  <img src="http://placehold.jp/300x250.png">-->
<!--              </picture>-->
<!--          </div>-->
<!--      </article>-->
    <!--============================================閉じるボタン（SPのみ）-->
    <div class="l-cont_foot sp-only"><button type="button" class="btn -close js-nav_toggle">メニューを閉じる</button></div>
  </section>
</aside>
