<?php get_header(); ?>
<?php global $ARTICLE_TOP_URL;?>
  <main>
    <?php

    $this_id = $post->ID;
    //訪問カウンター
    setPostViews($this_id);

    $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
    $post_cat = get_the_category($post->ID)[0];
    $data = [
        'pr'=> intval(get_post_meta($post->ID,'pr',true)),
        'link'=>make_root_path($post->guid),
        'title'=>wp_strip_all_tags($post->post_title),
        'cat_name'=> $post_cat->cat_name,
        'cat_slug'=> $post_cat->slug,
        'content'=>array('img'=>$img_url)
    ];
    $cat = get_the_category();
    $catid = $cat[0]->cat_ID; // ID
    $getCatURL = make_root_path(get_category_link($catid)); // カテゴリURL
    ?>

    <!--============================================パンくずリスト-->
    <ul class="breadcrumb">
      <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
      <li><a href="<?= $getCatURL; ?>"><?= $data['cat_name']; ?></a></li>
      <li class="pc-only"><?= mb_strimwidth(get_the_title(), 0, 56, "…", "UTF-8"); ?></li>
    </ul>

    <div class="main-contents">
      <!--============================================メインビジュアルエリア-->
      <div class="intro -single">
        <div class="intro_label">
          <span class="label -cat -<?= $data['cat_slug']; ?>"><?= $data['cat_name']; ?></span>
          <?php
          //タグ情報の生成
          $posttags = get_the_tags();
          if ($posttags || $data['pr'] === 1):?>
            <?php if(!empty($posttags)): foreach($posttags as $tag):?>
              <a class="label -tag" href="<?= make_root_path(esc_url(home_url('/?tag='.$tag->slug)));?>"><?= $tag->name;?></a>
            <?php endforeach; endif;?>
            <?php if($data['pr'] === 1): ?>
              <span class="label -pr">PR</span>
            <?php endif;?>
          <?php endif;?>
        </div>
        <div class="intro_title">
          <h1 class="title -single"><?= $data["title"]; ?></h1>
        </div>
      </div>

      <!--============================================更新日時＆SNSエリア-->
      <div class="single-info">
        <div class="single-info_date"><?= get_mtime('Y年n月j日'); ?></div>
        <?php $share_url = get_permalink(); ?>
        <div class="single-info_sns">
          <!-- /はてブボタン -->
          <a class="btn -sns -hatebu" href="//b.hatena.ne.jp/add?mode=confirm&url=<?= $share_url?>" onclick="return sns_window(this, 600, 1000);" title="はてなブックマークに登録">はてブ</a>
          <!-- /twitterボタン -->
          <a class="btn -sns -twitter" href="//twitter.com/share?text=<?= $data['title'];?>&url=<?= $share_url?>&via=wemo_blog" title="Twitterでシェア" onclick="return sns_window(this, 400, 600);">Twitter</a>
          <!-- /Facebookボタン -->
          <a class="btn -sns -facebook" href="//www.facebook.com/sharer.php?src=bm&u=<?= $share_url?>&t=<?= $data['title'];?>" title="Facebookでシェア" onclick="return sns_window(this, 800, 600);">LIKE<span class=""><?php if(function_exists('scc_get_share_facebook')) echo (scc_get_share_facebook()==0)?'':scc_get_share_facebook(); ?></span></a>
        </div>
      </div>

      <!--============================================メインビジュアル部分-->
      <div class="single-reading">
        <div class="single-reading_img"><img src="<?= CFS()->get('img_mv');?>" alt="" class="object-fit_img"/></div>
        <p class="single-reading_text"><?= CFS()->get('text_mv'); ?></p>
      </div>

      <!--============================================目次部分-->
      <dl class="single-toc">
        <dt class="single-toc_title"><span>目次</span></dt>
        <dd class="single-toc_data">
          <ul class="single-toc_list">
            <?php
            $fields = CFS()->get('add_chapter');
            $i = 1;//連番用変数
            foreach($fields as $field):
              ?>
              <li><a href="<?= '#chapter_'.$i ;?>"><?= $field['title_chapter']; ?></a></li>
              <?php $i++; endforeach; ?>
          </ul>
        </dd>
      </dl>

      <!--============================================chapter（章）部分-->
      <?php
      $fields = CFS()->get('add_chapter'); $i = 1;//連番用変数
      foreach($fields as $field):
        ?>
        <section class="single-cont" id="<?= 'chapter_'.$i ;?>">
          <div class="single-cont_title">
            <h2 class="title -chapter"><span><?= $field['title_chapter']; ?></span></h2>
          </div>
          <!--============================================section（節）部分-->
          <?php
          $sections = $field['add_section'];
          foreach ($sections as $section) :?>
            <div class="single-cont_section">
              <?php //$customfield = get_post_meta($field->ID, 'title_section', true); ?>
              <?php if(!empty($section['title_section'])): ?>
                <h3 class="title -section"><?= $section['title_section']; ?></h3>
              <?php endif; ?>
              <p class="single-cont_text">
                <?php if(!empty($section['img_section'])):?>
                  <span class="single-cont_img <?php if($section['img_size']) echo '-full';?>"><img src="<?= $section['img_section'];?>" alt=""/></span>
                <?php endif;?>
              <?= $section['text_section']; ?>
              </p>
            </div>
          <?php endforeach; ?>
        </section>
        <?php $i++; endforeach; ?>

      <!--============================================レコメンド表示部分ーおすすめ記事ー-->
      <!--============================================（別カテゴリの記事）-->

      <section class="single-recommend">
        <div class="l-cont -recommend">
          <div class="l-cont_title">
            <h2 class="title -recommend">あなたにおすすめの記事</h2>
          </div>
          <ul class="l-cont_list">
            <?php
            if (have_posts()) :
//            $args= build_args(array('pr'=>0,'sort'=>'DESC','count'=>4));
              $args= build_args(array('count'=>4,'cat'=>-$post_cat->term_id));
              $posts = get_posts($args);
              foreach($posts as $post):
                //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
                if (has_post_thumbnail()){
                  $thumbnail_id = get_post_thumbnail_id();
                  $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
                } else {
                  $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
                }
                //INIT::記事情報_カテゴリ
                $cat_post = get_the_category($post->ID)[0];
                if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
                $data = [
                    'pr'=> intval(get_post_meta($post->ID,'pr',true)),
                    'link'=>make_root_path($post->guid),
                    'title'=>text_ellipsis($post->post_title, 0, 60, "…", "UTF-8"),
                    'cat_name'=> $cat_post->cat_name,
                    'cat_slug'=>$cat_post->slug,
                    'content'=>array('img'=>$img_url)
                ];
                ?>
                <li>
                  <a href="<?= $data['link']; ?>">
                    <article class="cassette -vertical">
                      <div class="cassette_img">
                        <?php if(!empty($data['content']['img'])): ?>
                          <img src="<?= $data['content']['img'];?>" width="175" height="100" class="object-fit_img">
                        <?php else: ?>
                          <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                        <?php endif; ?>
                      </div>
                      <div class="cassette_detail">
                        <div class="cassette_label">
                          <span class="label -full -cat -<?= $data['cat_slug']; ?>"><?= $data['cat_name'];?></span>
                        </div>
                        <div class="cassette_title"><?= $data['title'];?></div>
                      </div>
                    </article>
                  </a>
                </li>
              <?php endforeach; else : ?>
              <li><?php _e('投稿はありません。'); ?></li>
            <?php endif; ?>
          </ul>

        </div>


        <!--============================================レコメンド表示部分ー関連記事ー-->
        <!--============================================（同一カテゴリの記事）-->

        <div class="l-cont -recommend">
          <div class="l-cont_title">
            <h2 class="title -recommend">関連した内容の記事</h2>
          </div>
          <ul class="l-cont_list">
            <?php
            if (have_posts()) :
//            $args= build_args(array('pr'=>0,'sort'=>'DESC','count'=>4));
              $args= build_args(array('count'=>4,'cat'=>$post_cat->term_id,'exclude'=>$this_id));
              $posts = get_posts($args);
              foreach($posts as $post):
                //INIT::記事情報_画像URL（任意でサムネイル画像を表示）
                if (has_post_thumbnail()){
                  $thumbnail_id = get_post_thumbnail_id();
                  $img_url = wp_get_attachment_image_src($thumbnail_id ,'medium')[0];
                } else {
                  $img_url = wp_get_attachment_url(get_post_meta($post->ID,'img_mv',true));
                }
                //INIT::記事情報_カテゴリ
                $cat_post = get_the_category($post->ID)[0];
                if($cat_post->slug==='live') $cat_post->cat_name = '住まいと暮らし';
                $data = [
                  'pr'=> intval(get_post_meta($post->ID,'pr',true)),
                  'link'=>make_root_path($post->guid),
                  'title'=>text_ellipsis($post->post_title, 0, 60, "…", "UTF-8"),
                  'cat_name'=> $cat_post->cat_name,
                  'cat_slug'=>$cat_post->slug,
                  'content'=>array('img'=>$img_url)
                ];
                ?>
                <li>
                  <a href="<?= $data['link']; ?>">
                    <article class="cassette -vertical">
                      <div class="cassette_img">
                        <?php if(!empty($data['content']['img'])): ?>
                          <img src="<?= $data['content']['img'];?>" width="175" height="100" class="object-fit_img">
                        <?php else: ?>
                          <img src="<?= get_template_directory_uri(); ?>/images/article-no_img.jpg" alt="no_image" class="object-fit_img">
                        <?php endif; ?>
                      </div>
                      <div class="cassette_detail">
                        <div class="cassette_label">
                          <span class="label -full -cat -<?= $data['cat_slug']; ?>"><?= $data['cat_name'];?></span>
                        </div>
                        <div class="cassette_title"><?= $data['title'];?></div>
                      </div>
                    </article>
                  </a>
                </li>
              <?php endforeach; else : ?>
              <li><?php _e('投稿はありません。'); ?></li>
            <?php endif; ?>
          </ul>
          <div class="l-cont_more"><a class="btn -more" href="<?= $getCatURL; ?>">すべての関連記事を見る</a></div>
        </div>


        <!--============================================カテゴリ一覧リンク-->

        <div class="l-cont -recommend">
          <div class="l-cont_title">
            <h2 class="title -recommend">他のカテゴリの記事を見る</h2>
          </div>
          <ul class="single-catLinks">

            <?php
            //INIT::カテゴリ情報取得
            $args = array('hide_empty'=>0,'orderby'=>'id');
            $categories = get_categories($args);
            foreach($categories as $category) :
              //INIT::カテゴリリンク
              $cat_link = make_root_path(get_category_link($category->term_id));
              if($category->slug==='live') $category->name = 'これからの住まいと暮らし';
              ?>
              <li class="single-catLinks_item">
                <?php if($category->term_id === $catid):?>
                  <span class="single-catLinks_link is-current"><?= $category->name; ?></span>
                <?php else:?>
                  <a class="single-catLinks_link" href="<?= $cat_link; ?>"><?= $category->name; ?></a>
                <?php endif;?>
              </li>
            <?php endforeach;?>
          </ul>
        </div>


      </section>

    </div>

    <?php get_sidebar(); ?>
    <!--============================================パンくずリスト（PCのみ）-->
    <ul class="breadcrumb -foot sp-only">
      <li><a href="<?= $ARTICLE_TOP_URL;?>">高齢者住宅ジャーナルTOP</a></li>
      <li><a href="<?= $getCatURL; ?>"><?= $data['cat_name']; ?></a></li>
    </ul>
  </main>
<?php get_footer(); ?>
